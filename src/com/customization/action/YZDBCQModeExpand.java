


package com.customization.action;


        import java.util.HashMap;
        import java.util.Map;

        import com.customization.commons.Console;
        import weaver.conn.RecordSet;
        import weaver.formmode.customjavacode.AbstractModeExpandJavaCodeNew;
        import weaver.general.Util;
        import weaver.hrm.User;
        import weaver.soa.workflow.request.*;


/**
 * 说明
 * 修改时
 * 类名要与文件名保持一致
 * class文件存放位置与路径保持一致。
 * 请把编译后的class文件，放在对应的目录中才能生效
 * 注意 同一路径下java名不能相同。
 * @author Administrator
 *
 */
public class YZDBCQModeExpand extends AbstractModeExpandJavaCodeNew {
    /**
     * 执行模块扩展动作
     * @param param
     *  param包含(但不限于)以下数据
     *  user 当前用户
     *  importtype 导入方式(仅在批量导入的接口动作会传输) 1 追加，2覆盖,3更新，获取方式(int)param.get("importtype")
     *  导入链接中拼接的特殊参数(仅在批量导入的接口动作会传输)，比如a=1，可通过param.get("a")获取参数值
     *  页面链接拼接的参数，比如b=2,可以通过param.get("b")来获取参数
     * @return
     */
    public Map<String, String> doModeExpand(Map<String, Object> param) {
    

        Map<String, String> result = new HashMap<String, String>();
        RecordSet rs=new RecordSet();
        RecordSet rst=new RecordSet();
        RecordSet rsi=new RecordSet();
        try {
            User user = (User)param.get("user");
            int billid = -1;//数据id
            int modeid = -1;//模块id
            RequestInfo requestInfo = (RequestInfo)param.get("RequestInfo");
            if(requestInfo!=null){
                billid = Util.getIntValue(requestInfo.getRequestid());
                modeid = Util.getIntValue(requestInfo.getWorkflowid());
                Console.log("getDBType:=="+rs.getDBType());
                if(billid>0&&modeid>0){
                    //------请在下面编写业务逻辑代码------
                    Console.log("billid:"+billid);
                    String sql="select dt.* from  uf_zbyzdbjl uf ,uf_zbyzdbjl_dt1  dt where uf.id ="+billid+" and uf.id =dt.mainid";
                    rs.execute(sql);
                    while (rs.next()){
                        int cqzy=rs.getInt("cqzy");
                        int cqrs =rs.getInt("cqrs");
                        int dtid= rs.getInt("id");

                        int hxcqrs =calculate(billid,cqrs,dtid,cqzy);
                        if(hxcqrs<=0){
                            continue;
                        }

                        String sqlwhere="";
                        sqlwhere+=" and zylx='"+cqzy+"'";//抽取专业条件
                        sqlwhere+=" and id not in (select xm from  uf_zbyzdbjl uf ,uf_zbyzdbjl_dt2  dt where uf.id ='"+billid+"' and uf.id =dt.mainid )";

                        String sec="select top "+hxcqrs+" * from uf_zbyzdbk where 1=1 "+sqlwhere+" order by newid() ";
                        if ("oracle".equals(rs.getDBType())) {
                            sec="SELECT * FROM (SELECT * FROM uf_zbyzdbk where 1=1 "+sqlwhere+" ORDER BY SYS_GUID()) WHERE ROWNUM <= "+hxcqrs;
                        }
                        Console.log("sec:"+sec);
                        rst.execute(sec);
                        while (rst.next()){
                            String xm=rst.getString("id");
                            String xb=rst.getString("xb");
                            String gzdw=rst.getString("gzdw");
                            String zw=rst.getString("zc");
                            String zjlb=rst.getString("zylx");

                            String insSQL="insert into   uf_zbyzdbjl_dt2 (mainid,xm,xb,gzdw,zw,zjlb)  VALUES('"+billid+"','"+xm+"','"+xb+"','"+gzdw+"','"+zw+"','"+zjlb+"')";

                            Console.log("insSQL:"+insSQL);
                            rsi.execute(insSQL);
                        }

                        calculate(billid,cqrs,dtid,cqzy);
                    }

                }
            }

        } catch (Exception e) {
            result.put("errmsg",":自定义出错信息"+e.toString());
            e.printStackTrace();
            result.put("flag", "false");
        }
        return result;
    }


    public int  calculate(int billid,int cqrs,int dtid,int zjlb){
        RecordSet rst=new RecordSet();
        int noreject=0;//未表态不参与的人员名单
        String secSql="select count(1) c from  uf_zbyzdbjl uf ,uf_zbyzdbjl_dt2  dt where  zjlb='"+zjlb+"' and uf.id ='"+billid+"' and (sfcj is null or  sfcj='' or sfcj=0) and uf.id =dt.mainid";
        rst.execute(secSql);
        Console.log("未表态不参与的人员名单:"+secSql);
        if(rst.first()){
            noreject=rst.getInt("c");
        }

        int cansure=0;//表态参与的
        secSql="select count(1) c from  uf_zbyzdbjl uf ,uf_zbyzdbjl_dt2  dt where zjlb='"+zjlb+"' and  uf.id ='"+billid+"' and sfcj=0 and uf.id =dt.mainid";
        rst.execute(secSql);
        Console.log("表态参与的:"+secSql);
        if(rst.first()){
            cansure=rst.getInt("c");
        }

        int extract=0;//抽取到的
        secSql="select count(1) c from  uf_zbyzdbjl uf ,uf_zbyzdbjl_dt2  dt where zjlb='"+zjlb+"' and  uf.id ='"+billid+"' and uf.id =dt.mainid";
        rst.execute(secSql);
        Console.log("表态参与的:"+secSql);
        if(rst.first()){
            extract=rst.getInt("c");
        }

        int hxcqrs= cqrs-noreject;
        secSql="update uf_zbyzdbjl_dt1 set qrrs='"+cansure+"',hxcqrs='"+hxcqrs+"',ycqrs='"+extract+"' where id='"+dtid+"'";
        Console.log("更新:"+secSql);
        rst.execute(secSql);

        return hxcqrs;

    }






}
