package com.customization.commons;


import org.apache.log4j.*;
import weaver.general.GCONST;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.IOException;


/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName Console.java
 * @Description 自己写的日志打印，UTF-8格式
 * @createTime 2022-03-16 15:10:00
 */
public class Console {

    //LoggerFactory.getLogger(DataSourceAction.class);

    static {



    }

    public static void log(Object obj) {

        StackTraceElement[] stacks = new Throwable().getStackTrace();
        String actionFileName = stacks[1].getFileName();
        write(obj, "info", actionFileName);
    }
    public static void log(String obj) {
        StackTraceElement[] stacks = new Throwable().getStackTrace();
        String actionFileName = stacks[1].getFileName();
        write(obj, "info", actionFileName);
    }

    public static void info(Object obj) {
        StackTraceElement[] stacks = new Throwable().getStackTrace();
        String actionFileName = stacks[1].getFileName();
        write(obj, "info", actionFileName);
    }

    public static void error(Object obj) {
        StackTraceElement[] stacks = new Throwable().getStackTrace();
        String actionFileName = stacks[1].getFileName();
        write(obj, "error", actionFileName);
    }

    public static void debug(Object obj) {
        StackTraceElement[] stacks = new Throwable().getStackTrace();
        String actionFileName = stacks[1].getFileName();
        write(obj, "debug", actionFileName);
    }


    private static void write(Object obj, String type, String actionFileName) {

        try {
            actionFileName = actionFileName.substring(0, actionFileName.indexOf("."));
            SimpleDateFormat CurrentDate = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat CurrentHour = new SimpleDateFormat("HH");
            Date date = new Date();
            String thisHour = CurrentHour.format(date);
            String thisDate = CurrentDate.format(date);
            String logPath = GCONST.getRootPath() + "log" + File.separatorChar + "devlog" + File.separatorChar + thisDate + File.separator + actionFileName;
            String dirStr = logPath + File.separatorChar + thisHour + ".log";
            Layout layout = new PatternLayout("%-d{HH:mm:ss,SSS}-[%p]  %m%n");
            //og4j2 additivity="false" log4j log4j.additivity.*.*=false
            //将路径配置在这里


            //2022/3/17 11:44 lth    解决打印日志会重复累计的问题，基础配置从配置文件获取

            PropertyConfigurator.configure(GCONST.getRootPath()+"WEB-INF"+File.separatorChar+"prop"+File.separatorChar+"log4jdev.properties");
            Logger  logger=org.apache.log4j.Logger.getLogger("devconsole");
            Appender appender = new FileAppender(layout, "" + dirStr);

          //  Filter _filter = appender.getFilter();
            logger.addAppender(appender);
            // 记录debug级别的信息
            if (type.equals("info")) {
                logger.info(obj);
            } else if (type.equals("debug")) {
                logger.debug(obj);
            } else if (type.equals("error")) {
                logger.error(obj);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
