package com.customization.api.uddi.service;

import java.util.Map;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName RequestService.java
 * @Description TODO
 * @createTime 2019-08-04 13:25:00
 */
public interface RequestService {

    String createRequest(Map<String, Object> params);
}
