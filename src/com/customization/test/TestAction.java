package com.customization.test;


import com.api.hrm.web.HrmResourceAddAction;
import com.customization.commons.Console;
import com.engine.workflow.service.impl.WorkflowTransferSetServiceImpl;
import com.mysql.cj.exceptions.ClosedOnExpiredPasswordException;


import com.customization.commons.LocalTestAction;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import okhttp3.*;
import org.junit.Assert;
import org.junit.Test;


import weaver.conn.RecordSet;

import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName TestAction.java
 * @Description TODO
 * @createTime 2020-05-15 10:55:00
 */
public class TestAction extends BaseTest {


    /**
     * 测试流程接口
     *
     * @throws Exception
     */
    @Test
    public void TestCustomAction() throws Exception {
        // HrmResourceAddAction

    }


    /**
     * 推送基础单据信息到报账系统
     *
     * @throws Exception
     */
    @Test
    public void TestSend2ImageAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3336644,
                "weaver.angel.action.fna.Send2ImageAction");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    /**
     * 推送基础单据信息到报账系统
     *
     * @throws Exception
     */
    @Test
    public void TestSendStatus2BzAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3090443,
                "weaver.angel.action.fna.SendStatus2BzAction");
        test.setLastOperator(13513);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");

    }

    /**
     * 推送基础单据信息到SSF系统
     *
     * @throws Exception
     */
    @Test
    public void TestSend2SSFAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3287596,
                "weaver.angel.action.fna.Send2SSFAction");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    /**
     * 推送单据支付状态信息到SSF系统
     *
     * @throws Exception
     */
    @Test
    public void TestSendStatus2SSFAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3150457,
                "weaver.angel.action.fna.SendStatus2SSFAction");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    /**
     * 更新发票状态到百望
     *
     * @throws Exception
     */
    @Test
    public void TestSendInvoiceStatus2BwAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3801103,
                "weaver.angel.action.fna.SendInvoiceStatus2BwAction");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }

    @Test
    public void TestAngelSAP_FI201() throws Exception {
        LocalTestAction test = new LocalTestAction(3300009,
                "weaver.angel.action.fna.AngelSAP_FI201");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }

    @Test
    public void TestAngelSAP_FI205() throws Exception {
        LocalTestAction test = new LocalTestAction(3300974,
                "weaver.angel.action.fna.AngelSAP_FI205");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }

    @Test
    public void TestAngelSAP_FI204_OIL_APPLY() throws Exception {
        LocalTestAction test = new LocalTestAction(3122430,
                "weaver.angel.action.fna.AngelSAP_FI204_OIL_APPLY");
        test.setLastOperator(5973);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    @Test
    public void TestAngelSAP_FI204() throws Exception {
        LocalTestAction test = new LocalTestAction(3486874,
                "weaver.angel.action.fna.AngelSAP_FI204");
        test.setLastOperator(6256);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }

    @Test
    public void TestTestAction() throws Exception {
        LocalTestAction test = new LocalTestAction(3281599,
                "weaver.angel.action.fna.TestAction");
        test.setLastOperator(6256);//操作人
        test.setRemark("系统管理员本地测试提交流程");
        test.setSrc("submit");//提交方式 不设置默认值:submit
        test.setSumitNext(false);//流程是否提交到下一步 默认值 false
        Assert.assertEquals(test.execute(), "1");
    }


    public static void main(String[] args) {
        String data = "{\"datas\":[{\"subcompanyid1span\":\"宜昌市政府\",\"subcompanyid\":\"5\",\"pinyinlastname\":\"李妍pinyinlastname\",\"departmentidspan\":\"市领导\",\"departmentid\":\"2\",\"icon\":\"/messager/images/icon_w_wev8.jpg\",\"subcompanyname\":\"宜昌市政府\",\"type\":\"resource\",\"supsubcompanyname\":\"\",\"title\":\"李妍|副总经理|宜昌市政府|市领导\",\"lastname\":\"李妍lastname\",\"jobtitlespan\":\"副总经理\",\"jobtitlename\":\"副总经理\",\"departmentname\":\"市领导\",\"lastnamespan\":\"李妍lastnamespan\",\"name\":\"李妍name\",\"id\":\"4\",\"displayKeys\":[\"lastnamespan\",\"jobtitlespan\"],\"nodeid\":\"resource_4x\",\"supsubcompanyid\":\"0\"}]}";
        System.out.println(data);
        JSONObject json = JSONObject.fromObject(data);
        if (json.containsKey("datas")) {
            JSONArray jsonArray = json.getJSONArray("datas");
            for (int i = 0; i < jsonArray.size(); i++) {
                json.getJSONArray("datas").getJSONObject(i).put("lastname",
                        json.getJSONArray("datas").getJSONObject(i).get("title"));
            }
        }
        System.out.println(json);
        JSONArray jsonArray = new JSONArray();


    }

}
