package com.customization.action;

import com.customization.commons.Console;

import com.customization.commons.CustomAction;
import com.customization.commons.WSBaseInfo;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.interfaces.workflow.action.Action;
import weaver.soa.workflow.request.RequestInfo;
import weaver.workflow.request.RequestManager;

import java.util.HashMap;
import java.util.Map;

public class AddSlabelFiledInfoAction   implements Action {

    private String classificationLevel;
    private String drafterName;
    private String issuerName;
    private String ClassifiedOrganizationName;
    private String classifiedTimeNum;
    private String classifiedTimeUnit;
    private String docfiled;


    private HashMap<String, String> mainMap;//主字段的值
    private HashMap<String, HashMap <String, HashMap<String, String>>> detailMap ;//所有明细表的值
    private WSBaseInfo wsBaseInfo ;//流程对象信息
    private String requestid;
    private String formid;
    private int wf_creater ;
    private int wf_formid  ;
    private int wf_isbill ;
    private String formtablename ;
    private int wf_user ;


    @Override
    public String execute(RequestInfo request) {
        CustomAction customAction= new CustomAction();
        Map<String,Object> map= customAction.initRequestData(request);
        mainMap= (HashMap<String, String>)map.get("mainMap");
        detailMap= (HashMap<String, HashMap<String, HashMap<String, String>>>) map.get("detailMap"); ;//所有明细表的值
        wsBaseInfo =(WSBaseInfo)map.get("wsBaseInfo");
        requestid=request.getRequestid();
        formid=""+request.getRequestManager().getFormid();
        wf_creater = (int)map.get("wf_creater");
        wf_formid  =(int)map.get("wf_formid");
        wf_isbill =(int)map.get("wf_isbill");
        formtablename =wsBaseInfo.getFormTablename();
        wf_user =(int)map.get("wf_user");
        String requestid = request.getRequestid();
        RequestManager rm = request.getRequestManager();
        try {
            String srcString = Util.null2String(rm.getSrc());

            /*
            srcString
            submit 提交
            reject 退回
            drawBack 强制收回
             */

            //当提交时执行
            if (srcString.equals("submit")) {

                Console.log( "mainFieldValuesMap"+mainMap);
                Console.log( "detailFieldValuesMap"+detailMap);
                String classificationLevelV=""+Util.getIntValue(Util.null2String(mainMap.get(this.getClassificationLevel())),5);

                String ClassifiedOrganizationNameV=Util.null2String(mainMap.get(this.getClassifiedOrganizationName()));

                String drafterNameV=Util.null2String(mainMap.get(this.getDrafterName()));
                String issuerNameV=Util.null2String(mainMap.get(this.getIssuerName()));
                String docid=Util.null2String(mainMap.get(this.getDocfiled()));

                String classifiedTimeNumV=Util.null2String(mainMap.get(this.getClassifiedTimeNum()));
                String classifiedTimeUnitV=Util.null2String(mainMap.get(this.getClassifiedTimeUnit()));

                RecordSet rs=new RecordSet();
                rs.execute("delete from cus_classified where requestid='"+requestid+"' and docid='"+docid+"'");
                rs.execute("insert into cus_classified (" +
                        "docid," +
                        "requestid," +
                        "classificationLevel," +
                        "drafterName,"+
                        "issuerName," +
                        "ClassifiedOrganizationName," +
                        "classifiedTimeNum," +
                        "classifiedTimeUnit) values('"+
                        docid+"','"+
                        requestid+"','" +
                        classificationLevelV+"','" +

                        drafterNameV+"','" +
                        issuerNameV+"','" +
                        ClassifiedOrganizationNameV+"','"+
                        classifiedTimeNumV+"','"+
                        classifiedTimeUnitV+"') ");
            }

        }catch (Exception e){
            //异常报错是填写异常信息，阻止流程继续流转
            request.getRequestManager().setMessageid("90001");
            request.getRequestManager().setMessagecontent(customAction.getErrorInfo()+"-"+rm.getRequestid()+" 系统异常");

                        return Action.FAILURE_AND_CONTINUE;
        }


        return Action.SUCCESS;
    }




    public String getClassificationLevel() {
        return classificationLevel;
    }

    public void setClassificationLevel(String classificationLevel) {
        this.classificationLevel = classificationLevel;
    }



    public String getDrafterName() {
        return drafterName;
    }

    public void setDrafterName(String drafterName) {
        this.drafterName = drafterName;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getClassifiedOrganizationName() {
        return ClassifiedOrganizationName;
    }

    public void setClassifiedOrganizationName(String classifiedOrganizationName) {
        ClassifiedOrganizationName = classifiedOrganizationName;
    }

    public String getClassifiedTimeNum() {
        return classifiedTimeNum;
    }

    public void setClassifiedTimeNum(String classifiedTimeNum) {
        this.classifiedTimeNum = classifiedTimeNum;
    }

    public String getClassifiedTimeUnit() {
        return classifiedTimeUnit;
    }

    public void setClassifiedTimeUnit(String classifiedTimeUnit) {
        this.classifiedTimeUnit = classifiedTimeUnit;
    }

    public String getDocfiled() {
        return docfiled;
    }

    public void setDocfiled(String docfiled) {
        this.docfiled = docfiled;
    }

}
