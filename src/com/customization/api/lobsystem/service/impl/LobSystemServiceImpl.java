package com.customization.api.lobsystem.service.impl;


import com.customization.api.lobsystem.cmd.getCreateDemoDataCmd;

import com.customization.api.lobsystem.service.LobSystemService;
import com.engine.core.impl.Service;

import java.util.Map;

public class LobSystemServiceImpl extends Service implements LobSystemService {

    @Override
    public Map<String, Object> getCreateDemoDataCmd(Map<String, Object> params) {
        return commandExecutor.execute(new getCreateDemoDataCmd(user, params));
    }

}
