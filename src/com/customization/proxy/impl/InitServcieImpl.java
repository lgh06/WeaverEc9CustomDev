package com.customization.proxy.impl;
import com.customization.commons.Console;
import com.weaverboot.frame.ioc.anno.classAnno.WeaSysInitComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaSysInit;


@WeaSysInitComponent("记录OA服务启动日志")
public class InitServcieImpl {
    @WeaSysInit(order = 1 ,description = "记录OA服务启动日志")
    public void init(){
        Console.log("start init");
    }
}