# 泛微EC9二次开发示例

>#### 一、介绍
此示例旨在更好的辅助大家在EC9的基础上进行二次开发,让我们更省时、更省心、更省力。  
我希望此示例为大家提供的更多的是一种思路，而不是一个工具。   
因为我的本职工作是一个项目经理，而非开发经理。  
在准备研究此示例之前，建议先通读泛微官方开发文档，包括但不限于：  
  
EC9技术站: [https://e-cloudstore.com/e9/index0.html](https://e-cloudstore.com/e9/index0.html)  
后端开发环境搭建:  [https://e-cloudstore.com/doc.html?appId=c6a9ae6e47b74d4da04c935ed51d177a](https://e-cloudstore.com/doc.html?appId=c6a9ae6e47b74d4da04c935ed51d177a)  
前端开发(_ecode代码编辑器_): [https://e-cloudstore.com/doc.html](https://e-cloudstore.com/doc.html)  

_**两大需求场景:**_  
**流程表单前端控制**    直达[https://e-cloudstore.com/doc.html?appId=98cb7a20fae34aa3a7e3a3381dd8764e](https://e-cloudstore.com/doc.html?appId=98cb7a20fae34aa3a7e3a3381dd8764e)   
**异构系统调用EC9接口** 直达[https://e-cloudstore.com/ec/api/applist/index.html](https://e-cloudstore.com/ec/api/applist/index.html)  


_**其他参考资料**_
EC9.0支持的可配置化的短信接口[查看](https://l1utaihong.gitee.io/weaverec9customdev/doc/SMS_INTERFACE.html)   
泛微OA产品部署HTTPS[查看](http://note.youdao.com/s/L8Qg8BFk)  
OA服务器运维脚本[查看](http://note.youdao.com/s/JhfblPlf)  
页面跳转支持传递登录人相关信息[查看](http://note.youdao.com/s/QYzRL8aK)

>#### 二、前言
一个开发经理可能会同时承担着多个客户的开发工作  
但EC不同KB版本的代码往往会有很大的差异   
如果全在一个工程中开发的话，代码混淆和出错的风险会很大  
故建议针对每个客户创建一个独立的工程  
通过本示例可以直连数据库进行相关调试  
减少服务器重启的次数，逃出时间沉没成本的陷阱   
不再需要反复的导出导入业务模块到本地  
测试过程更佳接近真实水平 


>#### 三、开发环境搭建
**_本示例可供学习使用，本示例中所有代码不会对标准产品产生任何影响。_**
推荐使用Idea进行java开发。  
**_二次开发无论是前端，还是后端都需要具备一定的开发能力。这很重要!!!，当然学习能力也更重要_** 
###### 1.通过git导入到本地开发工具中
* 提供git导入的方式创建
* 从零搭建，也只需要新建一个空的web项目即可，不需要引入任何框架。这样做的好处是有足够的空间来应付不同的客户。也不用频繁的在本地搭建不同客户的环境。   
###### 2.设置web目录  
在com.customization.test.BaseTest.java文件中设置项目所在的物理目录(主要是放置配置文件和日志打印的目录)
```
GCONST.setRootPath("D:\\devIdeaProjects\\WeaverEc9CustomDev\\web\\"); //window操作系统写法  
GCONST.setRootPath("/Users/liutaihong/IdeaProjects/WeaverEc9CustomDev/web/");//MacOS 操作系统写法  
```
###### 3.拷贝必要的jar包依赖
    这个很重要，需要有客户环境的KB补丁包保持一致
- 必须需要拷贝服务器上的/weaver/ecology/classbean目录本地项目中去  
- 至少需要拷贝服务器上的/weaver/ecology/WEB-INF/lib目录本地项目中去 
- 需要额外引入Junit4的jar包才能支持单元测试   
- 为了获得更好的支持建议每次更新eocde补丁包后需要拷贝ecode安装包 ecology_dev.zip里面 WEB-INF/lib下的jar包到JAVA工程中去
###### 4.拷贝必要的配置文件
- 需要拷贝服务器上的配置文件到/web/WEB-INF/prop目录目录下，包含但不限于有: 
```
/weaver/ecology/WEB-INF/prop/ecustom.properties  
/weaver/ecology/WEB-INF/prop/initCache.properties  
/weaver/ecology/WEB-INF/prop/weaver.properties  
/weaver/ecology/WEB-INF/prop/weaver_enableMultiLangEncoding_whiteList.properties  
/weaver/ecology/WEB-INF/prop/weaver_enableMultiLangEncoding_whiteList_new.properties  
/weaver/ecology/WEB-INF/prop/weaver_rtx.properties  
```
- 需要拷贝服务器上/weaver/ecology/WEB-INF/config/escache.xml到 WeaverEc9CustomDev/web/WEB-INF/prop/目录下
###### 5.拷贝并设置本地的数据库链接文件
本地数据库连接文件依旧为weaver.properties,具体目录在 WeaverEc9CustomDev/web/WEB-INF/prop/下面。确保和需要调试的服务器保持一致即可。
```
DriverClasses = oracle.jdbc.OracleDriver
#设置正确的JDBC链接 
ecology.url = jdbc:oracle:thin:@10.20.12.*:1521/ecology
#设置正确的数据库用户名
ecology.user = ecology
#设置正确的数据库密码
ecology.password = ecology
ecology.charset = ISO
ecology.maxconn = 300
ecology.minconn = 50
ecology.maxusecount = 6000
ecology.maxidletime = 600
ecology.maxalivetime = 10
ecology.checktime = 3600
ecology.isgoveproj = 0
LOG_FORMAT = yyyy.MM.dd'-'hh:mm:ss
DEBUG_MODE = false
```
###### 6.本地环境确认
经过上述环境的配置及调整，便可使用Junit4进行单元调试。例如单元测试 TestRecodUtil.java中的with方法可以查询到服务器的授权信息，即代表环境基本搭建好了，到此这个环境基本已经可以支持打断点来调试的。


>#### 使用说明

###### 1.流程Action调试实例
以Action com.customization.action.demo.MM050_node688_after，使用Junit4进行单元调试。  
用单元测试 TestAction.java中的testMM050_node688_after方法;  

