package com.customization.proxy.impl;


import com.api.hrm.web.HrmPasswordAction;
import com.customization.commons.Console;
import com.engine.kq.wfset.action.KqDeductionVacationAction;
import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceBefore;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaBeforeReplaceParam;
import weaver.general.Util;

import java.util.Map;

@WeaIocReplaceComponent("") 
public class ProxyCheckLogin4WeaSso {

    //这是接口前置方法，这个方法会在接口执行前执行
    //前值方法必须用@WeaReplaceBefore,这里面有两个参数，第一个叫value，是你的api地址
    //第二个参数叫order，如果你有很多方法拦截的是一个api，那么这个就决定了执行顺序
    //前置方法的参数为WeaBeforeReplaceParam 这个类，里面有四个参数，request，response，请求参数的map，api的地址


    @WeaReplaceBefore(value = "/api/hrm/login/checkLogin", order = 1, description = "第三方SSO登录")
    public void before(WeaBeforeReplaceParam weaBeforeReplaceParam){
        Map<String,Object> params=weaBeforeReplaceParam.getParamMap();
        Console.log("ParamMap:"+params.toString());
        String login_appid=Util.null2String(params.get("login_appid"));

        if(!login_appid.equals("")){
            params.put("islanguid",7);
            params.put("loginid","");
            params.put("userpassword","");
            params.put("dynamicPassword","");
            params.put("tokenAuthKey","");
            params.put("validatecode","");
            params.put("validateCodeKey","");
            params.put("logintype","");
            params.put("messages","");
            params.put("isie",false);
        }



    }


}
