package com.customization.api.lobsystem.cmd;

import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.core.interceptor.CommandContext;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.User;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class getCreateDemoDataCmd extends AbstractCommonCommand<Map<String, Object>> {

    public getCreateDemoDataCmd(User user, Map<String, Object> params) {
        this.user = user;
        this.params = params;

    }

    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {

        Map<String, Object> apidatas = new HashMap<String, Object>();
        apidatas.put("version", "20210829");
        apidatas.put("status", 0);
        apidatas.put("msg", "success");
       boolean showDesc=Util.null2String(params.get("showDesc")).equals("0")? true :false;


        try {


            String workfowid = Util.null2String(params.get("workflowid"));//流程ID
            if(workfowid.equals("")) {
                apidatas.put("msg", "workflowid不能为空");
                apidatas.put("status", 1);
                return apidatas;
            }
            String requestid = Util.null2String(params.get("requestid"));//流程ID
            apidatas.put("demodata", execDemo(workfowid,requestid,showDesc));
        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("msg", e.toString());
            apidatas.put("status", 1);
        }
        return apidatas;
    }


    public static JSONObject execDemo( String workfowid, String requestid,boolean showDesc){

        JSONObject result= new JSONObject();

        String requestName="";

        RecordSet rs=new RecordSet();
        RecordSet rst=new RecordSet();
        RecordSet rsd=new RecordSet();


        int formid=-1;
        String sql="select formid,workflowname from workflow_base where id='"+workfowid+"'";
        rst.execute(sql);
        rst.first();
        formid=rst.getInt("formid");
        requestName=rst.getString("workflowname");

        //查询明细表
        List<String > dtArr=new LinkedList<String>();
        sql="select DETAILTABLE from WORKFLOW_BILLFIELD where  billid='"+formid+"'  group by   DETAILTABLE order  by DETAILTABLE";
        rst.execute(sql);
        while (rst.next()){
            dtArr.add(Util.null2String(rst.getString("DETAILTABLE")));
        }


        JSONArray mainData=new JSONArray();


        JSONArray detailData=new JSONArray();


        for (String tablename:dtArr){
            JSONObject dtData=new JSONObject();
            JSONArray workflowRequestTableRecords=new JSONArray();
            JSONArray workflowRequestTableFields=new JSONArray();

            JSONObject workflowRequestTableRecordsObj=new JSONObject();


            boolean isDt=tablename.equals("")?false:true;

            //System.out.println(":"+tablename+";"+isDt+"--");




            String dtwhere="";
            String  secTable="formtable_main_"+Math.abs(formid);
            String secData=" select * from "+secTable+" where requestid='"+requestid+"'";//查询主表数据
            if(isDt){
                dtwhere= " and detailtable='"+tablename+"' ";
                //查询明细表数据
                secData="  select * from "+secTable+" fm,"+tablename+" dt  where dt.mainid=fm.id and fm.requestid='"+requestid+"'";
            }else{
                dtwhere= " and detailtable is null ";

            }

            rsd.execute(secData);//查询数据
            rsd.first();

            String sqls=" select wb.id as fieldid,wb.fieldname,wb.fielddbtype,wb.detailtable,ho.labelname,wc.description, wb.type,wb.fieldhtmltype,wb.qfws from ( " +
                    " select * from workflow_billfield where  billid='"+formid+"'  " +dtwhere+
                    " ) wb left join  htmllabelinfo ho on ho.indexid=wb.fieldlabel and  ho.languageid= 7  " +
                    " left join  wf_browser_config  wc on wc.type=to_char(wb.type) " +
                    "where labelname  not  like '%删除%' "+
                    " order by wb.dsporder ";
            System.out.println("sqls:"+sqls);
            rs.execute(sqls);

            while (rs.next()){
                String fieldname= rs.getString("fieldname");
                String fielddbtype= rs.getString("fielddbtype");
                String detailtable= rs.getString("detailtable");
                String labelname=Util.null2String( rs.getString("labelname"));
                String description= Util.null2String(rs.getString("description"));
                String type= rs.getString("type");
                int fieldhtmltype= rs.getInt("fieldhtmltype");
                String fieldid=rs.getString("fieldid");
                String qfws=rs.getString("qfws");
                //System.out.println(fieldname+";"+labelname+"；"+description+";"+type+";"+labelname+";"+tablename);

                JSONObject obj=new JSONObject();//字段信息
                obj.put("fieldName",fieldname);
                obj.put("fieldValue",rsd.getString(fieldname));


                JSONObject objDesc=new JSONObject();//字段说明

                objDesc.put("labelname",labelname);
                objDesc.put("dbtype",fielddbtype);
                objDesc.put("type",type);



                String showtype="";
                if(fieldhtmltype==1){
                    showtype+="单行文本框";
                    if(type.equals("1")){
                        showtype+="-文本";
                    }else  if(type.equals("2")){
                        showtype+="-整数";

                    }else  if(type.equals("3")){

                        showtype+="-"+qfws+"位浮点数";
                    }


                }else if(fieldhtmltype==2){
                    showtype+="多行文本框";

                }else if(fieldhtmltype==3){
                    showtype+="浏览按钮";

                    //if(description.equals("")){
                    {
                        if(type.equals("1")) {
                            showtype += "-人力资源";
                            objDesc.put("comment","传递人力资源接口中的 userid 字段");
                        }else  if(type.equals("2")) {
                            showtype += "-日期";
                            objDesc.put("comment","日期格式 yyyy-MM-dd");
                        }else if(type.equals("3")) {
                            showtype += "-会议室联系单";
                            objDesc.put("comment","");
                        }else if(type.equals("4")) {
                            showtype += "-部门";
                            objDesc.put("comment","传递部门接口中的 departmentid 字段");
                        }else if(type.equals("5")) {
                            showtype += "-仓库";
                            objDesc.put("comment","");
                        }else if(type.equals("6")) {
                            showtype += "-成本中心";
                            objDesc.put("comment","");
                        }else if(type.equals("7")) {
                            showtype += "-客户";
                            objDesc.put("comment","");
                        }else if(type.equals("8")) {
                            showtype += "-项目";
                            objDesc.put("comment","");
                        }else if(type.equals("9")) {
                            showtype += "-文档";
                            objDesc.put("comment","");
                        }else if(type.equals("10")) {
                            showtype += "-入库方式";
                            objDesc.put("comment","");
                        } else if(type.equals("11")) {
                            showtype += "-出库方式";
                            objDesc.put("comment","");
                        } else if(type.equals("12")) {
                            showtype += "-币种";
                            objDesc.put("comment","");
                        } else if(type.equals("13")) {
                            showtype += "-资产种类";
                            objDesc.put("comment","");
                        } else if(type.equals("14")) {
                            showtype += "-科目－全部";
                            objDesc.put("comment","");
                        } else if(type.equals("15")) {
                            showtype += "-科目－明细";
                            objDesc.put("comment","");
                        } else if(type.equals("16")) {
                            showtype += "-请求";
                            objDesc.put("comment","传递流程接口中的requestid字段");
                        }else if(type.equals("17")) {
                            showtype += "-多人力资源";
                            objDesc.put("comment","传递人力资源接口中的 userid 字段，多个值时用英文逗号隔开");
                        }   else if(type.equals("18")) {
                            showtype += "-多客户";
                            objDesc.put("comment","");
                        }else if(type.equals("19")) {
                            showtype += "-时间";
                            objDesc.put("comment","时间格式 HH:mm");
                        }else if(type.equals("57")) {
                            showtype += "-多部门";
                            objDesc.put("comment","传递部门接口中的 departmentid 字段，多个值时用英文逗号隔开");
                        } else if(type.equals("164")) {
                            showtype += "-分部";
                            objDesc.put("comment","传递分部接口中的 subcompanyid 字段 ");
                        } else if(type.equals("194")) {
                            showtype += "-多分部";
                            objDesc.put("comment","传递分部接口中的 subcompanyid 字段，多个值时用英文逗号隔开");
                        }else if(type.equals("152")) {
                            showtype += "-多请求";
                            objDesc.put("comment","传递流程接口中的requestid字段，多个值时用英文逗号隔开");
                        }else if(type.equals("171")) {
                            showtype += "-归档请求";
                            objDesc.put("comment","传递流程接口中的requestid字段，多个值时用英文逗号隔开");
                        }else if(type.equals("58")){
                            showtype += "-城市";
                            objDesc.put("comment","传递城市接口中的 cityid");
                        }else if(type.equals("226")){
                            showtype += "-系统集成单选浏览";
                            objDesc.put("comment","传递SAP主数据的主键，具体需要根据实际表单进行约定");
                        }

                    }



                    /*else{
                        showtype+="-"+description;
                        JSONObject browObj=new JSONObject();
                        browObj.put("value",rsd.getString(fieldname));
                        browObj.put("name","");
                        obj.put("fieldValue",browObj);
                    }*/



                }else if(fieldhtmltype==4){
                    showtype+="Check框";

                }else if(fieldhtmltype==5){
                    showtype+="选择框";
                    String secSelect="select selectvalue,selectname from workflow_selectitem where fieldid='"+fieldid+"' and (cancel=0 or cancel is null)  order by listorder asc";
                    rst.execute(secSelect);

                    objDesc.put("comment","该字段fieldValue传值时 selectList 中的 key值");
                    JSONObject selectList=new JSONObject();
                    while(rst.next()){
                        selectList.put(rst.getString("selectvalue"),rst.getString("selectname"));
                    }

                    objDesc.put("selectList",selectList);

                }else if(fieldhtmltype==6){
                    showtype+="附件";

                    JSONArray fileArr=new JSONArray();
                    JSONObject fileObj=new JSONObject();
                    fileObj.put("fileName","");
                    fileObj.put("filePath","");
                    fileArr.add(fileObj);

                    obj.put("fieldValue",fileArr);

                    objDesc.put("comment","这是一个附件字段，支持http链接和base64传值");

                }





                objDesc.put("showtype",showtype);

                if(showDesc){
                    obj.put("fieldDesc",objDesc);
                }

                 /*附件
                    fieldValue:[
                    {
                        "filePath": "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1577426926378&di=0084fc19f5cb90fc2286aab5ca8c343e&imgtype=0&src=http%3A%2F%2Fpic.51yuansu.com%2Fpic2%2Fcover%2F00%2F41%2F80%2F581369c715701_610.jpg",
                            "fileName": "timg.jpg"
                    },
                    {
                        "filePath": "http://192.168.7.216:8080/weaver/weaver.file.FileDownload?download=1&fileid=3819049&authStr=dmlld0NoYWluPTIyNTkzOTA1fG1haW5pZD0yMjU5MzkwNXw=&authSignatureStr=81b08f6e0b8ee06dcc1b43fda8204bdf&requestid=22593905&f_weaver_belongto_userid=4548&f_weaver_belongto_usertype=0",
                            "fileName": "自由节点视图mysql.txt"
                    }
                    ]*/

                if(isDt){ //明细表
                    workflowRequestTableFields.add(obj);
                }else{//主表
                    mainData.add(obj);
                }


            }
            //  System.out.println(isDt+";workflowRequestTableFields："+workflowRequestTableFields);



            if(isDt){
                workflowRequestTableRecordsObj.put("recordOrder","0");
                workflowRequestTableRecordsObj.put("workflowRequestTableFields",workflowRequestTableFields);
                workflowRequestTableRecords.add(workflowRequestTableRecordsObj);
                dtData.put("tableDBName",tablename);
                dtData.put("workflowRequestTableRecords",workflowRequestTableRecords);
                detailData.add(dtData);
            }
        }






        JSONObject otherParams=new JSONObject();
        otherParams.put("isnextflow","0");//新建流程是否默认提交到第二节点，可选值为[0 ：不流转 1：流转 (默认)]
        otherParams.put("delReqFlowFaild","1");//新建流程失败是否默认删除流程，可选值为[0 ：不删除 1：删除 (默认)]
        otherParams.put("requestSecLevel","");//"流程密级， 开启密级后生效， 默认公开",


        result.put("detailData",detailData);
        result.put("mainData",mainData);
        result.put("otherParams",otherParams);
        result.put("requestLevel","");
        result.put("requestName",requestName);
        result.put("workflowId",workfowid);



        return  result;
    }


}