package com.customization.cronjob;

public class KQScheduleSignEntity {
    String loginid = "";
    String workcode = "";
    String lastname = "";
    String signdate = "";
    String signtime = "";
    String clientaddress = "";
    String longitude = "";
    String latitude = "";
    String addr = "";
    String memo = "";

    String uploadDate="";

    public KQScheduleSignEntity() {
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getLoginid() {
        return this.loginid;
    }

    public void setLoginid(String var1) {
        this.loginid = var1;
    }

    public String getWorkcode() {
        return this.workcode;
    }

    public void setWorkcode(String var1) {
        this.workcode = var1;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String var1) {
        this.lastname = var1;
    }

    public String getSigndate() {
        return this.signdate;
    }

    public void setSigndate(String var1) {
        this.signdate = var1;
    }

    public String getSigntime() {
        return this.signtime;
    }

    public void setSigntime(String var1) {
        this.signtime = var1;
    }

    public String getClientaddress() {
        return this.clientaddress;
    }

    public void setClientaddress(String var1) {
        this.clientaddress = var1;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String var1) {
        this.longitude = var1;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String var1) {
        this.latitude = var1;
    }

    public String getAddr() {
        return this.addr;
    }

    public void setAddr(String var1) {
        this.addr = var1;
    }

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String var1) {
        this.memo = var1;
    }
}
