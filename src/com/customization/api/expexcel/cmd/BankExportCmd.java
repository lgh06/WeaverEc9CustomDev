package com.customization.api.expexcel.cmd;


import com.cloudstore.eccom.pc.table.WeaTable;
import com.cloudstore.eccom.pc.table.WeaTableColumn;
import com.cloudstore.eccom.result.WeaResultMsg;
import com.customization.commons.Console;
import com.engine.common.biz.AbstractCommonCommand;
import com.engine.common.entity.BizLogContext;
import com.engine.core.interceptor.CommandContext;
import weaver.conn.RecordSet;
import weaver.general.PageIdConst;
import weaver.general.Util;
import weaver.hrm.User;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class BankExportCmd extends AbstractCommonCommand<Map<String,Object>> {

    public BankExportCmd(User user, Map<String,Object> params) {
        this.user = user;
        this.params = params;
    }


    @Override
    public BizLogContext getLogContext() {
        return null;
    }

    @Override
    public Map<String, Object> execute(CommandContext commandContext) {
        Map<String, Object> apidatas = new HashMap<String, Object>();

        /*
        if(!HrmUserVarify.checkUserRight("LogView:View", user)){
            apidatas.put("hasRight", false);
            return apidatas;
        }
        */

        try {
            Console.log("exec "+this.getClass().getName());
            //返回消息结构体
            WeaResultMsg result = new WeaResultMsg(false);

            String pageID = UUID.randomUUID().toString();
            String pageUid = pageID + "_" + user.getUID();
            String pageSize = PageIdConst.getPageSize(pageID, user.getUID());
            String sqlwhere = " 1=1 ";

            //新建一个weatable
            WeaTable table = new WeaTable();
            table.setPageUID(pageUid);

            table.setPageID(pageID);

            table.setPagesize(pageSize);

            String fileds = " * ";
            table.setBackfields(fileds);

            Console.log("params:"+params.toString());
            //"业务参考号|收款人编号|收款人帐号|收款人名称|收方开户支行|收款人所在省|收款人所在市|
            // 收方邮件地址|收方移动电话|币种|付款分行|结算方式|业务种类|付方帐号|期望日|期望时间|
            // 用途|金额|收方行号|收方开户银行|业务摘要";
            String tablesql="select *  from hrmresource where id<120";//jsonResult.toString()t
            tablesql="SELECT  pzh as 业务参考号,'' as 收款人编号,  yhzh as  收款人帐号  ,(select lastname from hrmresource where id=bxr ) as 收款人名称 ,khh as 收方开户支行 ,(SELECT sf FROM formtable_main_189 where id=nvl(trim(ss),0)) as 收款人所在省,(SELECT ds FROM  formtable_main_188 where id=nvl(trim(dis),0)) as 收款人所在市," +
                    " '' as 收方邮件地址,'' as 收方移动电话,(SELECT selectname FROM  workflow_selectitem where fieldid =9407 and selectvalue=bz) as 币种, '' as 付款分行, '快速' as 结算方式, '普通汇兑' as 业务种类, '' as 付方帐号, TO_CHAR(SYSDATE,'YYYYMMDD') as 期望日, '080000' as 期望时间," +
                    " cksy as 用途,zfhjhje as 金额 ,'' as 收方行号,(select yhlx from formtable_main_341  where id = formtable_main_139.yhlx ) as 收方开户银行,lcbh as 业务摘要 FROM  formtable_main_139 where requestid in (2219856,2218219)";
            if(!Util.null2String(params.get("sql")).equals("")){
               String sql=params.get("sql").toString();
               tablesql=new String(Base64.getDecoder().decode(sql), "UTF-8");
            }
            tablesql=tablesql.replace("delete","deletenew")
                    .replace("update","updatenew")
                    .replace("insert","insertnew");

            String fromtable="(select rownum ,t.* from ( "+tablesql+"  ) t  ) tt";
            //String fromtable="("+tablesql+" ) tt";
            table.setSqlform(fromtable);
            table.setSqlwhere(sqlwhere);
            table.setSqlorderby("rownum");
            table.setSqlprimarykey("rownum");
            table.setSqlisdistinct("false");
            String  sqls="select "+ fileds+ "from "+fromtable+" where "+sqlwhere;
            

            Console.log(sqls);

            RecordSet recordSet= new RecordSet();
            recordSet.execute(sqls);
            String [] nametitles = recordSet.getColumnName();
            for (int i=0;i<nametitles.length;i++){
                Console.log("field:"+nametitles[i]);
                if(!nametitles[i].equals("ROWNUM")) {
                    table.getColumns().add(new WeaTableColumn("20%", nametitles[i], nametitles[i]));
                }
            }

            //设置左侧check默认不存在
            table.setCheckboxList(null);
            table.setCheckboxpopedom(null);
            result.putAll(table.makeDataResult());
            result.put("hasRight", true);
            result.success();
            apidatas = result.getResultMap();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return apidatas;
    }
}
