package com.customization.action;

import com.engine.hrm.service.impl.HrmStateChangeServiceImpl;
import weaver.conn.RecordSet;
import weaver.general.BaseBean;
import weaver.hrm.User;
import weaver.interfaces.workflow.action.Action;
import weaver.soa.workflow.request.RequestInfo;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;

/**
 * 人事变更流程
 */
public class Action20201005092640   implements Action{


    private String requestId;
    protected String workflowId;
    public String UserId;
    /**
     * 流程路径节点后选择aciton后,会在节点提交后执行此方法。
     */
    @Override
    public String execute(RequestInfo request) {
        // 获取requestid
        String requestId = request.getRequestid();
        String workflowId="";
        String UserId="";
        // 获取表单名称
        String tablename = request.getRequestManager().getBillTableName();
        // 查找表单相关内容
        RecordSet mainrs = new RecordSet();
        mainrs.execute("select * from "+tablename+" where requestid =  "+requestId);
        mainrs.next();
        // 获取主表ID
        String mainid = mainrs.getString("id");
        // 获取表单中员工姓名ID
        int userId = mainrs.getInt("ygxm");
        // 获取员工变动日期
        String changedate = mainrs.getString("bgrq");
        // 获取员工变动类型
        String changeType = mainrs.getString("bdlx");
        // 定义变量hrmMap存储相关参数
        Map<String, Object> hrmMap = new HashMap<>();
        hrmMap.put("tempresourceid",userId);
        hrmMap.put("changedate",changedate);
        // 获取上下文变量
        ServletContext localServletContext = request.getRequestManager().getRequest().getSession(true).getServletContext();

        // 调用人员离职的方法
        new HrmStateChangeServiceImpl().saveHrmDismiss(hrmMap, new User(userId), localServletContext);
        
        return Action.SUCCESS;
    }
}
