package weaver.file;


/**
 * Title:        hpsales
 * Description:  for hp sales system
 * Copyright:    Copyright (c) 2001
 * Company:      weaver
 *
 * @author liuyu, Charoes Huang
 * @version 1.0 ,2004-6-25
 */
import com.api.doc.util.DocEncryptUtil;
import com.customization.action.utils.ActionUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import weaver.alioss.AliOSSObjectManager;
import weaver.conn.RecordSet;
import weaver.docs.docs.DocManager;
import weaver.docs.docs.ImageFileIdUpdate;
import weaver.email.MailCommonUtils;
import weaver.email.service.MailAliOSSService;
import weaver.file.multipart.DefaultFileRenamePolicy;
import weaver.file.multipart.MultipartRequest;
import weaver.file.multipart.UploadedFile;
import weaver.file.util.FileManager;
import weaver.file.util.FileSuffixCheckUtil;
import weaver.file.util.PicCompression;
import weaver.filter.XssUtil;
import weaver.general.BaseBean;
import weaver.general.GCONST;
import weaver.general.StaticObj;
import weaver.general.Util;
import weaver.hrm.User;
import weaver.security.util.SecurityMethodUtil;
import weaver.system.SystemComInfo;
import weaver.docs.docs.util.UrlEncoderUtils;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipInputStream;

public class FileUpload extends BaseBean {
    private static ImageFileIdUpdate imageFileIdUpdate = new ImageFileIdUpdate();


    private MultipartRequest mpdata = null;
    HttpServletRequest request = null;
    private String[] filenames = null;
    private InputStream source = null;
    private ArrayList filesizes = new ArrayList();
    private ArrayList imagewidth = new ArrayList();
    private ArrayList imageheight = new ArrayList();
    private ArrayList delfilerealpaths = new ArrayList();
    private int mailid;
    private boolean needimagewidth = false;
    private boolean needzip = false;
    private boolean needzipencrypt = false;
    private String isaesencrypt = "0";
    private String aescode = "";
    private String remoteAddr = "";
    private XssUtil xss = null;

    public FileUpload(HttpServletRequest request) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request);
        this.request = request;
        this.xss = new XssUtil();
    }

    public FileUpload(HttpServletRequest request, String encode) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, encode);
        this.request = request;
        this.xss = new XssUtil();
    }

    public FileUpload(HttpServletRequest request, String encode, boolean iszip, String isEmail) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request) && "1".equals(isEmail)) mpdata = getEmailAttachment(request, encode, iszip);
        this.request = request;
        this.xss = new XssUtil();
    }

    public FileUpload(HttpServletRequest request, String encode, boolean iszip) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, encode, iszip);
        this.request = request;
        this.xss = new XssUtil();
    }

    public FileUpload(HttpServletRequest request, String encode, boolean iszip, boolean isoriginal) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, encode, iszip, isoriginal);
        this.request = request;
        this.xss = new XssUtil();
    }

    //modify by mackjoe at 2005-11-28 td3282 获得request对象
    public HttpServletRequest getRequest() {
        return request;
    }

    //for license upload by chenyingjie 2003-06-26
    public FileUpload(HttpServletRequest request, boolean iszip) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, iszip);
        this.request = request;
    }

    public FileUpload(HttpServletRequest request, boolean iszip, boolean isaesencrypt) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, iszip, isaesencrypt);
        this.request = request;
    }


    //for homepage image upload by dongping 2006-8-24
    public FileUpload(HttpServletRequest request, boolean iszip, String strDirAddr) {
        this.remoteAddr = request.getRemoteAddr();
        if (isMultipartData(request)) mpdata = getAttachment(request, iszip, strDirAddr);
        this.request = request;
    }

    //html模板图片保存
    public FileUpload(HttpServletRequest request, boolean iszip, boolean encoding, String strDirAddr) {
        this.remoteAddr = Util.getIpAddr(request);
        if (isMultipartData(request)) mpdata = getAttachment(request, iszip, encoding, strDirAddr);
        this.request = request;
    }

    public Hashtable getUploadImgNames() {
        String el = "", imgpath = "", imgname = "";
        Hashtable ht = new Hashtable();
        for (Enumeration e = mpdata.getFileUploadNames(); e.hasMoreElements(); ) {
            el = (String) e.nextElement();
            if (el.indexOf("docimages_") == -1) continue;
            imgpath = Util.null2String(mpdata.getFilePath(el));
            imgname = Util.null2String(mpdata.getFileName(el));
            if (imgpath.equals("") || imgname.equals("")) continue;
            String elNumber = el.substring(el.indexOf("_") + 1, el.length());
            ht.put(elNumber, imgpath + imgname);

        }
        return ht;
    }

    public Hashtable getUploadFileNames() {
        String el = "", imgpath = "", imgname = "";
        Hashtable ht = new Hashtable();
        for (Enumeration e = mpdata.getFileUploadNames(); e.hasMoreElements(); ) {
            el = (String) e.nextElement();
            UploadedFile uploadedFile = mpdata.getUploadedFile(el);
            ht.put(el, uploadedFile);
        }
        return ht;
    }

    //modify by xhheng @20050315 for 流程附件上传
    public String getRemoteAddr() {
        return remoteAddr;
    }

    public String getParameter(String key) throws RuntimeException {
        if (!isMultipartData(request)) return Util.null2String(request.getParameter(key));
        if (mpdata == null) return "";
//        return Util.null2String(mpdata.getParameter(key)) ;

        try {
            String value = Util.null2String(mpdata.getParameter(key));
            if (!value.equals("")) {//特殊参数需要从paramsMap中获取
                if (value.startsWith(XssUtil.__RANDOM__)) {
                    //xssUtil.cacheKey(Util.null2String(Thread.currentThread().hashCode()),value);
                    return xss.get(value);
                }
            }
            return new String(value.getBytes("ISO8859_1"), "UTF-8");
        } catch (Exception ex) {
            return "";
        }
    }


    public String getParameter2(String key) {
        if (!isMultipartData(request)) return Util.null2String(request.getParameter(key));
        if (mpdata == null) return "";
        String value = Util.null2String(mpdata.getParameter(key));
        return value;
    }

    /**
     * 本方法返回值不会对null进行特殊处理，如果是null则直接返回null，
     * 如果是空字符串("")则直接返回空字符串("")。
     *
     * @param key
     * @return
     */
    public String getParameter3(String key) {
        if (!isMultipartData(request)) {
            return request.getParameter(key);
        }
        if (mpdata == null) {
            return null;
        }

        String value = mpdata.getParameter(key);
        if (value == null) {
            return null;
        }

        try {
            return new String(value.getBytes("ISO8859_1"), "UTF-8");
        } catch (Exception ex) {
            return value;
        }
    }

    public String[] getParameters(String key) {
        if (!isMultipartData(request)) return request.getParameterValues(key);
        if (mpdata == null) return null;
        String[] values = mpdata.getParameterValues(key);
        return values;
    }

    public Enumeration getParameterNames() {
        if (!isMultipartData(request)) return request.getParameterNames();
        if (mpdata == null) return null;
        return mpdata.getParameterNames();
    }

    public String[] getParameterValues(String name) {
        if (!isMultipartData(request)) return request.getParameterValues(name);
        if (mpdata == null) return null;
        String[] values = mpdata.getParameterValues(name);
        return values;
    }

    public String[] getParameterValues2(String name) {
        String[] values = null;
        if (!isMultipartData(request)) {
            values = request.getParameterValues(name);
            String[] multivalues = new String[values.length];
            try {
                for (int i = 0; i < values.length; i++) {
                    multivalues[i] = new String(Util.null2String(values[i]).getBytes("ISO8859_1"), "UTF-8");
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return multivalues;
        }
        if (mpdata == null) return null;
        values = mpdata.getParameterValues(name);
        String[] multivalues = new String[values.length];
        try {
            for (int i = 0; i < values.length; i++) {
                multivalues[i] = new String(Util.null2String(values[i]).getBytes("ISO8859_1"), "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return multivalues;
    }


    public String getFileName() {
        return this.filenames[0].replace("'", "’");
    }

    public String[] getFileNames() {
        return this.filenames;
    }

    public void setFileNames(String[] filenames) {
        this.filenames = filenames;
    }

    public int getFileSize() {
        return Util.getIntValue((String) this.filesizes.get(0));
    }

    public int[] getFileSizes() {
        int[] filesizearr = new int[filesizes.size()];
        for (int i = 0; i < filesizes.size(); i++) filesizearr[i] = Util.getIntValue((String) this.filesizes.get(i));
        return filesizearr;
    }

    public void setFileSizes(ArrayList filesizes) {
        this.filesizes = filesizes;
    }

    public int getWidth() {
        return Util.getIntValue((String) this.imagewidth.get(0));
    }

    public int getHeight() {
        return Util.getIntValue((String) this.imageheight.get(0));
    }

    public int[] getWidths() {
        int[] imagewidtharr = new int[imagewidth.size()];
        for (int i = 0; i < imagewidth.size(); i++)
            imagewidtharr[i] = Util.getIntValue((String) this.imagewidth.get(i));
        return imagewidtharr;
    }

    public int[] getHeights() {
        int[] imageheightarr = new int[imageheight.size()];
        for (int i = 0; i < imageheight.size(); i++)
            imageheightarr[i] = Util.getIntValue((String) this.imageheight.get(i));
        return imageheightarr;
    }

    public void setMailid(int mailid) {
        this.mailid = mailid;
    }

    public int getMailid() {
        return mailid;
    }

    public void needImagewidth(boolean needimagewidth) {
        this.needimagewidth = needimagewidth;
    }

    public ArrayList getDelFilerealpaths() {
        return delfilerealpaths;
    }


    public String uploadFiles(String uploadname) {
        String[] uploadnames = new String[1];
        uploadnames[0] = uploadname;
        String paramFileName = this.getParameter("name"); //获取参数当中的文件名
        //String[] filenames = uploadFiles(uploadnames) ;
        String[] filenames = uploadFiles(uploadnames, paramFileName);
        if (filenames == null || filenames.length < 1) {
            return null;
        }
        return filenames[0];
    }

    //add by liuy 20190705 start
    /* 上传文件文件名IOS系统出现错误问题修复
     *
     */
    public String[] uploadFiles(String[] uploadnames, String paramFileName) {
        if (mpdata == null) return null;

        int upload_numbers = uploadnames.length;
        String[] fileids = new String[upload_numbers];
        this.filenames = new String[upload_numbers];
        for (int i = 0; i < upload_numbers; i++) {
            filenames[i] = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadnames[i]));
            if (filenames[i] == null || "".equals(filenames[i])) {
                return fileids;
            }
            //满足三个条件，否则走else；
            //1 请求参数未包含文件名
            //2 有请求参数并且和上传文件流里的文件名参数一致
            //3 值不一致，但是文件流里的文件名为file，防止 IOS篡改
            if (StringUtils.isBlank(paramFileName) || paramFileName.equals(filenames[i]) || (!paramFileName.equals(filenames[i]) && !"file".equals(filenames[i]))) {
                fileids[i] = saveFile(uploadnames[i], mpdata);
            } else {
                filenames[i] = paramFileName; //修改文件名
                fileids[i] = saveFile(uploadnames[i], paramFileName, mpdata);
            }

        }
        return fileids;
    }

    public String[] uploadFiles(String[] uploadnames) {
        if (mpdata == null) return null;
        int upload_numbers = uploadnames.length;
        String[] fileids = new String[upload_numbers];
        this.filenames = new String[upload_numbers];
        for (int i = 0; i < upload_numbers; i++) {
            filenames[i] = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadnames[i]));
            if (filenames[i] != null && !filenames[i].equals("")) {
                fileids[i] = saveFile(uploadnames[i], mpdata);
            }
        }
        return fileids;
    }

    /**
     * 处理邮件模块上传的附件存储
     *
     * @param uploadnames
     * @return
     */
    public String uploadFilesToEmail(String uploadname) {
        String[] uploadnames = new String[1];
        uploadnames[0] = uploadname;
        String[] filenames = uploadFilesToEmail(uploadnames);
        return filenames[0];
    }

    /**
     * 处理邮件模块上传的附件存储
     *
     * @param uploadnames
     * @return
     */
    public String[] uploadFilesToEmail(String[] uploadnames) {
        if (mpdata == null) return null;
        int upload_numbers = uploadnames.length;
        String[] fileids = new String[upload_numbers];
        this.filenames = new String[upload_numbers];
        for (int i = 0; i < upload_numbers; i++) {
            filenames[i] = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadnames[i]));
            if (filenames[i] != null && !filenames[i].equals("")) {
                fileids[i] = saveFileToEmail(uploadnames[i], mpdata);
            }
        }
        return fileids;
    }

    /**
     * 保存邮件附件记录到mailresourcefile表。
     *
     * @param uploadname
     * @param mpdata
     * @return
     */
    private synchronized String saveFileToEmail(String uploadname, MultipartRequest mpdata) {
        String imageid = "";
        String filepath = mpdata.getFilePath(uploadname);
        String filename = mpdata.getFileName(uploadname);
        String originalfilename = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadname));
        writeLog("-saveFileToEmail-filename:" + filename + "-originalfilename:" + originalfilename);
        originalfilename = StringEscapeUtils.unescapeHtml(originalfilename);
        writeLog("-saveFileToEmail-unescapeHtml-originalfilename:" + originalfilename);
        String contenttype = mpdata.getContentType(uploadname);
        long filesize = mpdata.getFileSize(uploadname);
        String filerealpath = filepath + filename;
        // WeavermailComInfo wmc = new WeavermailComInfo() ;
        String isfileattrachment = "1";// wmc.getCurrentFileAttachment() ;
        String fileContentId = "";// wmc.getCurrentFileContentId();
        String isEncoded = "";// wmc.getCurrentFilenameencode();
        String secretLevel = Util.null2s(getParameter("secretLevel"), "4");
        String secretDeadline = Util.null2s(getParameter("secretLevelValidity"), "");

        //判断实体文件是否存在，如果不存在，保存失败。
        File file = new File(filerealpath);
        if (!file.exists()) {
            writeLog("检测到实体文件不存在，附件上传失败.originalfilename=" + originalfilename + ",filerealpath=" + filerealpath);
            return imageid;
        }

        String iszip = "0";
        String isencrypt = "0";
        if (needzip) iszip = "1";
        if (needzipencrypt) isencrypt = "1";

        originalfilename = escapeSpecialCharactersForFileName(originalfilename);

        String originalfilenameflag=originalfilename;
        String useragent = Util.null2String(request.getHeader("User-Agent"));
        if(useragent.contains("Mobile") || useragent.contains("E-Mobile")){
            try {
                originalfilename = URLDecoder.decode(originalfilename,"utf-8");
            } catch (Exception e) {
                originalfilename = originalfilenameflag;
            }
        }

        RecordSet rs = new RecordSet();
        char separator = Util.getSeparator();
        String mailFileUUID = MailCommonUtils.getRandomUUID();
        String para = "0" + separator + originalfilename + separator + contenttype + separator + filerealpath
                + separator + iszip + separator + isencrypt + separator + isfileattrachment + separator + fileContentId
                + separator + isEncoded + separator + String.valueOf(filesize) + separator + mailFileUUID;
        rs.executeProc("MailResourceFile_Insert", para);
        rs.executeQuery("select id from MailResourceFile where mrf_uuid = ?", mailFileUUID);
        if (rs.next()) {
            imageid = rs.getString("id");
        }
        // qc 1491982 完整性校验所需参数，上传时计算文件签名等信息入库，下载时通过此参数进行计算校验文件是否被篡改
        String signInfo = "";
        String hashInfo = "";
        // 允许进行加密
        if (DocEncryptUtil.allowEncrypt(originalfilename)) {
            // 加密成功时再对文件签名且标识为已加密，否则signInfo为空，下载时不进行解密
            String[] signData = DocEncryptUtil.signFile(filerealpath);
            if (signData != null) {
                hashInfo = signData[0];
                signInfo = signData[1];
            }
        }

        // 更新加密信息
        rs.executeUpdate("update MailResourceFile set isaesencrypt=?, signinfo = ?,hashinfo = ?, aescode=?, secretLevel=?, secretDeadline=? where id=?", isaesencrypt,signInfo,hashInfo,aescode, secretLevel, secretDeadline, imageid);

        //处理oss存储逻辑
        MailAliOSSService mailAliOSSService = new MailAliOSSService();
        mailAliOSSService.updateFileToOSSByUUID(mailFileUUID);

        return imageid;
    }

    /**
     * 过滤附件名称里的特殊字符，防止入库 是出现 问号?
     *
     * @param fileName
     * @return
     */
    private String escapeSpecialCharactersForFileName(String fileName) {
        fileName = Util.null2String(fileName);

        fileName = fileName.replaceAll("(\r\n|\r|\n|\n\r)", "").replaceAll("\t", ""); // 替换换行符，tab符

        //替换英文半角字符,防止半角字符展示为？问号 https://tbatm.iteye.com/blog/2196068
        byte bytes[] = {(byte) 0xC2, (byte) 0xA0};
        try {
            String UTFSpace = new String(bytes, "utf-8");
            fileName = fileName.replaceAll(UTFSpace, " ");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return fileName;
    }


    /**
     * 处理邮件模块上传的附件存储
     *
     * @param uploadnames
     * @param withsave
     * @return
     */
    public ArrayList uploadFilesToMail(String[] uploadnames, String withsave) {
        if (mpdata == null) return null;
        int upload_numbers = uploadnames.length;
        ArrayList filecontents = new ArrayList();
        this.filenames = new String[upload_numbers];
        for (int i = 0; i < upload_numbers; i++) {
            String tempfilename = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadnames[i]));
            if (tempfilename != null && !tempfilename.equals("")) {
                filenames[i] = tempfilename;
                filecontents.add(getFileContent(uploadnames[i], mpdata, withsave));
            }
        }
        return filecontents;
    }


    private MultipartRequest getAttachment(HttpServletRequest req) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, "", isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    private MultipartRequest getAttachment(HttpServletRequest req, String encoding) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, encoding, isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    private MultipartRequest getEmailAttachment(HttpServletRequest req, String encoding, boolean iszip) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();

                //               String createdir = getCreateDir(syscominfo.getFilesystem()) ;
                String createdir = GCONST.getRootPath() + "filesystem" + File.separatorChar;
                createdir = getCreateDir(createdir + File.separatorChar);
                RecordSet rs = new RecordSet();
                rs.execute("select filePath from MailConfigureInfo");
                while (rs.next()) {
                    String emailpath = rs.getString("filePath");
                    if (!"".equals(emailpath)) {
                        createdir = getCreateDir(emailpath + File.separatorChar);
                    }
                }

                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                needzip = iszip;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, encoding, isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    protected MultipartRequest getAttachment(HttpServletRequest req, String encoding, boolean iszip) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                needzip = iszip;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, encoding, isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    private MultipartRequest getAttachment(HttpServletRequest req, String encoding, boolean iszip, boolean original) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = null;
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                isaesencrypt = "0";
                aescode = Util.getRandomString(13);
                needzip = false;
                if (!original) {
                    isaesencrypt = syscominfo.getIsaesencrypt();
                    aescode = Util.getRandomString(13);
                    needzip = iszip;
                }

                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, encoding, isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    //for license upload by chenyingjie 2003-06-26
    private MultipartRequest getAttachment(HttpServletRequest req, boolean iszip) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                if (!iszip) needzip = false;
                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, "", isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    private MultipartRequest getAttachment(HttpServletRequest req, boolean iszip, boolean isaesencryptBoolean) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                String createdir = getCreateDir(syscominfo.getFilesystem());
                if (isaesencryptBoolean) {
                    isaesencrypt = syscominfo.getIsaesencrypt();
                    aescode = Util.getRandomString(13);
                }
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //       if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                if (!iszip) needzip = false;
                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, "", isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    public List getFiles() {
        if (mpdata == null) return null;
        return mpdata.getFiles();
    }

    //for homepage edit by dongping 2006-08-24
    private MultipartRequest getAttachment(HttpServletRequest req, boolean iszip, String strDirAddr) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                //String createdir = getCreateDir(syscominfo.getFilesystem()) ;
                isaesencrypt = syscominfo.getIsaesencrypt();
                aescode = Util.getRandomString(13);
                String createdir = getCreateDir(GCONST.getRootPath() + strDirAddr);
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                if (!iszip) needzip = false;
                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, "", isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    //html模板图片保存
    private MultipartRequest getAttachment(HttpServletRequest req, boolean iszip, boolean encoding, String strDirAddr) {
        if (isMultipartData(req))
            try {
                DefaultFileRenamePolicy defpolicy = new DefaultFileRenamePolicy();
                SystemComInfo syscominfo = new SystemComInfo();
                //String createdir = getCreateDir(syscominfo.getFilesystem()) ;
                isaesencrypt = syscominfo.getIsaesencrypt();
                if (!encoding) isaesencrypt = "0";
                aescode = Util.getRandomString(13);
                String createdir = getCreateDir(GCONST.getRootPath() + strDirAddr);
                if ((syscominfo.getNeedzip()).equals("1")) needzip = true;
                //if( (syscominfo.getNeedzipencrypt()).equals("1") ) needzipencrypt = true ;

                if (!iszip) needzip = false;
                return new MultipartRequest(req, createdir, req.getContentLength(), defpolicy, needzip, needzipencrypt, "", isaesencrypt, aescode);
            } catch (Exception ex) {
                writeLog(ex);
                return null;
            }
        return null;
    }

    private InputStream getFileContent(String uploadname, MultipartRequest mpdata, String withsave) {
        if (withsave.equals("1") || withsave.equals("2")) {

            String filepath = mpdata.getFilePath(uploadname);
            String filename = mpdata.getFileName(uploadname);
            String originalfilename = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadname));
            String contenttype = mpdata.getContentType(uploadname);
            long filesize = mpdata.getFileSize(uploadname);
            String filerealpath = filepath + filename;
            //WeavermailComInfo wmc = new WeavermailComInfo() ;
            String isfileattrachment = "1";//wmc.getCurrentFileAttachment() ;
            String fileContentId = "";//wmc.getCurrentFileContentId();
            String isEncoded = "";//wmc.getCurrentFilenameencode();

            String iszip = "0";
            String isencrypt = "0";
            if (needzip) iszip = "1";
            if (needzipencrypt) isencrypt = "1";

            RecordSet rs = new RecordSet();
            char separator = Util.getSeparator();

            String para = "" + mailid + separator + originalfilename + separator
                    + contenttype + separator + filerealpath + separator
                    + iszip + separator + isencrypt + separator + isfileattrachment + separator
                    + fileContentId + separator + isEncoded + separator + String.valueOf(filesize);

            rs.executeProc("MailResourceFile_Insert", para);
        } else {
            String filepath = mpdata.getFilePath(uploadname);
            String filename = mpdata.getFileName(uploadname);
            String filerealpath = filepath + filename;
            delfilerealpaths.add(filerealpath);
        }

        try {
            File thefile = mpdata.getFile(uploadname);
            if (needzip) {
                ZipInputStream zin = new ZipInputStream(new FileInputStream(thefile));
                if (zin.getNextEntry() != null) source = new BufferedInputStream(zin);
            } else source = new BufferedInputStream(new FileInputStream(thefile));
        } catch (Exception e) {
            writeLog(e);
        }

        return source;

            /* 原有的存储附件数据在数据库中的方式

            boolean isoracle = (statement.getDBType()).equals("oracle") ;

            try   {
                statement = new ConnStatement();

                File thefile = mpdata.getFile(uploadname) ;
                int fileLength = new Long(thefile.length()).intValue();
                source = new BufferedInputStream(new FileInputStream(thefile),500*1024) ;

                String sql =  "" ;

                if( isoracle) {
                    sql = "insert into MailResourceFile(mailid,filename,attachfile,filetype) values(?,?,empty_blob(),?)";
                    statement.setStatementSql(sql);
                    statement.setInt(1,mailid);
                    statement.setString(2,filename) ;
                    statement.setString(3,contenttype) ;
                    statement.executeUpdate();

                    sql = "select rownum,  attachfile from ( select attachfile from MailResourceFile order by id desc ) where rownum = 1 " ;
                    statement.setStatementSql(sql);
                    statement.executeQuery();
                    statement.next() ;
                    BLOB theblob = statement.getBlob(2) ;

                    int bytesize = theblob.getBufferSize() ;
                    byte[] buffer = new byte[bytesize] ;
                    OutputStream outstream = theblob.getBinaryOutputStream() ;
                    int length = -1 ;

                    while((length = source.read(buffer)) != -1)
                        outstream.write(buffer, 0 , length) ;
                    outstream.close() ;
                }
                else {
                    sql = "insert into MailResourceFile(mailid,filename,attachfile,filetype) values(?,?,?,?)";
                    statement.setStatementSql(sql);
                    statement.setInt(1,mailid);
                    statement.setString(2,filename) ;
                    statement.setBinaryStream(3,source,fileLength);
                    statement.setString(4,contenttype) ;
                    statement.executeUpdate();
                }

                source.close() ;
                thefile.delete() ;

                if(withsave.equals("1")) {
                    sql = "select max(id) from MailResourceFile " ;
                    statement.setStatementSql(sql);
                    statement.executeQuery();
                    statement.next() ;
                    int fileid = statement.getInt(1) ;

                    sql = "select attachfile from MailResourceFile where id = " + fileid;
                    statement.setStatementSql(sql);
                    statement.executeQuery();
                    statement.next() ;
                    byte[] imagebyte = null ;
                    if( isoracle ) imagebyte = statement.getBlobByte("attachfile") ;
                    else imagebyte = statement.getBytes("attachfile") ;
                    source = new BufferedInputStream(new ByteArrayInputStream(imagebyte),500*1024) ;
                }
                statement.close() ;


            }catch(Exception ex){}
        }
        return source ;  */
    }


    private synchronized String saveFile(String uploadname, MultipartRequest mpdata) {

        int imageid = 0;
        String filepath = mpdata.getFilePath(uploadname);
        String filename = mpdata.getFileName(uploadname);
        String originalfilename = SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadname));
        String contenttype = mpdata.getContentType(uploadname);
        long filesize = mpdata.getFileSize(uploadname);
        new ActionUtil().writeLogNew("originalfilename" + originalfilename);
        String message = Util.null2String(request.getAttribute("emessage_savepath"));
        String _imagefilename = Util.null2String(getParameter("imagefilename"));
        String __originalfilename = Util.null2String(originalfilename);
        String _extname = __originalfilename.contains(".") ? __originalfilename.substring(__originalfilename.indexOf(".")) : "";
        if (!_imagefilename.isEmpty() && ("".equals(_extname) || _imagefilename.endsWith(_extname))) {
            originalfilename = _imagefilename;
        }
        String ts = Util.null2String(getParameter("ts"));
        if (!"".equals(ts)) {
            long t1 = getLongValue(ts, -1);
            Calendar c = Calendar.getInstance();
            long t2 = c.getTimeInMillis();
            if (t1 > 0) {
                int limitUploadTime = Util.getIntValue(getPropValue("doc_upload_suffix_limit", "limitUploadTime"), 60);
                if (limitUploadTime > 0 && t2 - t1 > limitUploadTime * 60 * 1000) {
                    writeLog("830^^^^^^^^^(文件上传请求过时ts=" + ts + ";limitUploadTime=" + limitUploadTime + ";)(" + originalfilename + ")^^^^^^^^^^^filepath=" + filepath + filename);
                    return null;
                }
            }


        }
        new ActionUtil().writeLogNew("filename:" + filename + ";originalfilename:" + originalfilename + ";contenttype:" + contenttype + ";_imagefilename:" + _imagefilename + ";__originalfilename:" + __originalfilename + ";_extname:" + _extname);

        if (filesize == 0) {
            writeLog("^^^^^^^^^(文件大小为0)(" + originalfilename + ")^^^^^^^^^^^filepath=" + filepath + filename);
            return null;
        }

        String filerealpath = filepath + filename;
        int fieldId = Util.getIntValue(getParameter("fieldId"));
        int workflowId = Util.getIntValue(getParameter("workflowId"));
        /*
         * QC675390概述：前后台增加附件上传格式控制
         * 如果上传文件的文件名后缀不符合配置文件中的要求，则不上传
         */
        if (!FileSuffixCheckUtil.isEnableUpload(originalfilename) || (fieldId > 0 && workflowId > 0 && !checkFileSuffix(originalfilename, fieldId, workflowId))) {
            writeLog("^^^^^^^^^(文件拦截)(" + originalfilename + ")^^^^^^^^^^^filepath=" + filepath + filename);
            File _tempFile = new File(filerealpath);
            // 如果文件存在，且是文件，删除
            if (_tempFile.exists() && _tempFile.isFile()) {
                FileSecurityUtil.deleteFile(_tempFile);
                // _tempFile.delete();
            }
            return null;
        }
        MineType mimeType = new MineType();
        InputStream in = null;
        boolean checkIsPass = true;
        try {
            in = new FileInputStream(new File(filerealpath));
            if (!mimeType.checkFileType(in)) {
                writeLog("非法的文件，上传的文件MIME为：" + mimeType.getMime());
                checkIsPass = false;
                return null;
            }
        }catch(Exception e){
            writeLog("获取文件流失败" , e);
            return null;
        } finally{

            if(in != null){
                try{ in.close();}catch(Exception e){}
            }
            in = null;
            if(!checkIsPass){
                File _tempFile = new File(filerealpath);
                // 如果文件存在，且是文件，删除
                if (_tempFile.exists() && _tempFile.isFile()) {
                    FileSecurityUtil.deleteFile(_tempFile);
                }
            }
        }
        //tagtag需要压缩图片
        //System.out.println("contenttype:"+ contenttype);
        //System.out.println("filerealpath:"+ filerealpath);
        String needCompressionPic = (String) this.request.getAttribute("needCompressionPic");
        //zxh 在配置项里查找需要压缩的图片的后缀名
        RecordSet rsCompressionPic= new RecordSet();
        String imageNeedCompressionPic = "," + Util.null2String(rsCompressionPic.getPropValue("sapXIsoap", "needCompressionPic"), "") + ",";
        //图片大于200KB的设置需要压缩
        if (filesize > 200 * 1024 && imageNeedCompressionPic.contains("," +_extname + ",")) {
            needCompressionPic = "1";
        } else {
            needCompressionPic = "0";
        }
        if ("1".equals(needCompressionPic) && !needzip) {
            PicCompression picCompression = new PicCompression();
            //图片大小大于5M的用用加强压缩比例
            if (filesize > 5 * 1024 * 1024) {
                String s = picCompression.compress(filerealpath, 1024, 800, 0.7f);
            } else {
                String s = picCompression.compress(filerealpath, 1024, 800, 0.9f);
            }
        }


        String imagefileused = "1";
        String iszip = "0";
        String isencrypt = "0";
        if (needzip) iszip = "1";
        if (needzipencrypt) isencrypt = "1";


        RecordSet rs = new RecordSet();
        char separator = Util.getSeparator();

        //rs.executeProc("SequenceIndex_SelectFileid" , "" );
        //if( rs.next() ) imageid = Util.getIntValue(rs.getString(1));
        imageid = imageFileIdUpdate.getImageFileNewId();
        String originalfilenameflag=originalfilename;
        String useragent = Util.null2String(request.getHeader("User-Agent"));
        if(useragent.contains("Mobile") || useragent.contains("E-Mobile")){
            try {
                originalfilename = URLDecoder.decode(originalfilename,"utf-8");
            } catch (Exception e) {
                originalfilename = originalfilenameflag;
                //e.printStackTrace();
            }
        }

        String para = "" + imageid + separator + originalfilename + separator
                + contenttype + separator + imagefileused + separator + filerealpath + separator
                + iszip + separator + isencrypt + separator + filesize;

        rs.executeProc("ImageFile_Insert", para);
        AliOSSObjectManager aliOSSObjectManager = new AliOSSObjectManager();
        String tokenKey = aliOSSObjectManager.getTokenKeyByFileRealPath(filerealpath);
        String secretLevel = Util.null2s(getParameter("secretLevel"), DocManager.DEFAILT_SECRET_LEVEL + "");
        String secretValidity = Util.null2s(getParameter("secretLevelValidity"), "");
        String name = Util.null2s(getParameter("name"), "");

        // qc 1491982 完整性校验所需参数，上传时计算文件签名等信息入库，下载时通过此参数进行计算校验文件是否被篡改
        String signInfo = "";
        String hashInfo = "";
        // 允许进行加密
        if (DocEncryptUtil.allowEncrypt(originalfilename)) {
            String[] signData = DocEncryptUtil.signFile(filerealpath);
            if (signData != null) {
                hashInfo = signData[0];
                signInfo = signData[1];
            }
        }
        String sql = "update imagefile set isaesencrypt=" + isaesencrypt + ", aescode='" + aescode + "',signinfo='"+signInfo+ "',hashinfo='"+hashInfo+ "',TokenKey='" + tokenKey + "',secretLevel=" + secretLevel + ",secretValidity='" + secretValidity + "' where imagefileid=" + imageid;
//        rs.writeLog("fileupload1111-----fileid:"+imageid+"---name:"+name+"--secretLevel:"+secretLevel+"--secretValidity:"+secretValidity+"--sql"+sql);
        // 更新加密信息
        rs.executeUpdate(sql);


        /**上传解密 预留接口  start*/
        FileManager.upload(request,imageid,filerealpath,aescode,tokenKey,iszip,isaesencrypt,mpdata);
        /**上传解密 预留接口  end*/

        aliOSSObjectManager.uploadFile(filerealpath, originalfilename, iszip, isaesencrypt, aescode);

        try {                                        // add by liuyu to get image file width and height
            if (contenttype.indexOf("image") != -1 && needimagewidth) {
                File thefile = mpdata.getFile(uploadname);
                long fileLength = thefile.length();
                filesizes.add("" + fileLength);

                if (needzip) {
                    ZipInputStream zin = new ZipInputStream(new FileInputStream(thefile));
                    if (zin.getNextEntry() != null) source = new BufferedInputStream(zin);
                } else source = new BufferedInputStream(new FileInputStream(thefile));

                if (isaesencrypt.equals("1")) {
                    source = AESCoder.decrypt(source, aescode);
                }
                //byte[] imagebyte = new byte[64*1024] ;
                //StringBuffer buf = new StringBuffer();
                //while (source.read(imagebyte, 0, imagebyte.length) != -1) buf.append(imagebyte);
                //ByteArraySeekableStream bs =  new ByteArraySeekableStream((buf.toString()).getBytes()) ;
                //RenderedOp bimage = JAI.create("stream", bs);
                //imagewidth.add(""+bimage.getWidth()) ;
                //imageheight.add(""+bimage.getHeight()) ;


                ImageInfo ii = new ImageInfo();
                ii.setInput(source);
                if (!ii.check()) {
                    imagewidth.add("0");
                    imageheight.add("0");
                } else {
                    imagewidth.add("" + ii.getWidth());
                    imageheight.add("" + ii.getHeight());
                }
            } else {
                imagewidth.add("0");
                imageheight.add("0");
                filesizes.add("0");
            }
        } catch (Exception imgex) {
            imagewidth.add("0");
            imageheight.add("0");
            filesizes.add("0");
        }

        return imageid + "";

        /* 原有的存储附件数据在数据库中的方式

        try {
          File thefile = mpdata.getFile(uploadname) ;
          int fileLength = new Long(thefile.length()).intValue();
          source = new BufferedInputStream(new FileInputStream(thefile),500*1024) ;

          filesizes.add(""+fileLength) ;

          statement = new ConnStatement();
          boolean isoracle = (statement.getDBType()).equals("oracle") ;

          String sql = "select currentid from SequenceIndex where indexdesc='imagefileid'";
          statement.setStatementSql(sql);
          statement.executeQuery();

          if(statement.next()){
              imageid = statement.getInt("currentid");
          }

          sql = "update SequenceIndex set currentid=? where indexdesc='imagefileid'";
          statement.setStatementSql(sql);
          statement.setInt(1,imageid+1);
          statement.executeUpdate();

          if( isoracle) {
              sql = "insert into ImageFile values(?,?,?,empty_blob(),?)";
              statement.setStatementSql(sql);
              statement.setInt(1,imageid);
              statement.setString(2,filename) ;
              statement.setString(3,contenttype) ;
              statement.setInt(4,1);
              statement.executeUpdate();

              sql = "select imagefile from ImageFile where imagefileid = " + imageid ;
              statement.setStatementSql(sql);
              statement.executeQuery();
              statement.next() ;
              BLOB theblob = statement.getBlob(1) ;

              int bytesize = theblob.getBufferSize() ;
              byte[] buffer = new byte[bytesize] ;
              OutputStream outstream = theblob.getBinaryOutputStream() ;
              int length = -1 ;

              while((length = source.read(buffer)) != -1)
                outstream.write(buffer, 0 , length) ;
              outstream.close() ;
          }
          else {
              sql = "insert into ImageFile values(?,?,?,?,?)";
              statement.setStatementSql(sql);
              statement.setInt(1,imageid);
              statement.setString(2,filename) ;
              statement.setString(3,contenttype) ;
              statement.setBinaryStream(4,source,fileLength);
              statement.setInt(5,1);
              statement.executeUpdate();
          }

          source.close();
          thefile.delete() ;

          try   {                                        // add by liuyu to get image file width and height
              if(contenttype.indexOf("image") != -1 && needimagewidth)  {
                  sql = "select imagefile from ImageFile where imagefileid = "+imageid;
                  statement.setStatementSql(sql);
                  statement.executeQuery();
                  statement.next() ;
                  byte[] imagebyte = null ;
                  if( isoracle ) imagebyte = statement.getBlobByte("imagefile") ;
                  else imagebyte = statement.getBytes("imagefile") ;
                  ByteArraySeekableStream bs =  new ByteArraySeekableStream(imagebyte) ;
                  RenderedOp bimage = JAI.create("stream", bs);
                  imagewidth.add(""+bimage.getWidth()) ;
                  imageheight.add(""+bimage.getHeight()) ;
              }
              else {
                  imagewidth.add("0") ;
                  imageheight.add("0") ;
              }
          }
          catch (Exception imgex) {
              imagewidth.add("0") ;
              imageheight.add("0") ;
          }

          statement.close() ;
        }
        catch (Exception ex) { writeLog(ex); }

        return imageid+""; */
    }

    private synchronized String saveFile(String uploadname, String paramFileName, MultipartRequest mpdata) {

        int imageid = 0;
        String filepath = mpdata.getFilePath(uploadname);
        String filename = mpdata.getFileName(uploadname);
        String originalfilename = paramFileName;
        String contenttype = mpdata.getContentType(uploadname);
        long filesize = mpdata.getFileSize(uploadname);

        String _imagefilename = Util.null2String(getParameter("imagefilename"));
        String __originalfilename = Util.null2String(originalfilename);
        String _extname = __originalfilename.contains(".") ? __originalfilename.substring(__originalfilename.indexOf(".")) : "";
        if (!_imagefilename.isEmpty() && ("".equals(_extname) || _imagefilename.endsWith(_extname))) {
            originalfilename = _imagefilename;
        }
        String ts = Util.null2String(getParameter("ts"));
        if(!"".equals(ts)){
            long t1=getLongValue(ts,-1);
            Calendar c = Calendar.getInstance();
            long t2 = c.getTimeInMillis();
            if(t1>0){
                int limitUploadTime= Util.getIntValue(getPropValue("doc_upload_suffix_limit","limitUploadTime"),60);
                if(limitUploadTime>0 && t2 - t1 > limitUploadTime*60*1000){
                    writeLog("1058^^^^^^^^^(文件上传请求过时ts="+ts+";limitUploadTime="+limitUploadTime+";)("+originalfilename+")^^^^^^^^^^^filepath=" + filepath + filename);
                    return null;
                }
            }
        }
        if (filesize == 0) {
            writeLog("^^^^^^^^^(文件大小为0)(" + originalfilename + ")^^^^^^^^^^^filepath=" + filepath + filename);
            return null;
        }

        String filerealpath = filepath + filename;

        //tagtag需要压缩图片
        //System.out.println("contenttype:"+ contenttype);
        //System.out.println("filerealpath:"+ filerealpath);
        String needCompressionPic = (String) this.request.getAttribute("needCompressionPic");
        if ("1".equals(needCompressionPic) && !needzip) {
            PicCompression picCompression = new PicCompression();
            String s = picCompression.compress(filerealpath, 1280, 1024, 1);
        }


        String imagefileused = "1";
        String iszip = "0";
        String isencrypt = "0";
        if (needzip) iszip = "1";
        if (needzipencrypt) isencrypt = "1";


        RecordSet rs = new RecordSet();
        char separator = Util.getSeparator();

        //rs.executeProc("SequenceIndex_SelectFileid" , "" );
        //if( rs.next() ) imageid = Util.getIntValue(rs.getString(1));
        imageid = imageFileIdUpdate.getImageFileNewId();
        String originalfilenameflag=originalfilename;
        String useragent = Util.null2String(request.getHeader("User-Agent"));
        if(useragent.contains("Mobile") || useragent.contains("E-Mobile")){
            try {
                originalfilename = URLDecoder.decode(originalfilename,"utf-8");
            } catch (Exception e) {
                originalfilename=originalfilenameflag;
                //e.printStackTrace();
            }
        }
        String para = "" + imageid + separator + originalfilename + separator
                + contenttype + separator + imagefileused + separator + filerealpath + separator
                + iszip + separator + isencrypt + separator + filesize;

        rs.executeProc("ImageFile_Insert", para);
        AliOSSObjectManager aliOSSObjectManager = new AliOSSObjectManager();
        String tokenKey = aliOSSObjectManager.getTokenKeyByFileRealPath(filerealpath);
        String secretLevel = Util.null2s(getParameter("secretLevel"), DocManager.DEFAILT_SECRET_LEVEL + "");
        String secretValidity = Util.null2s(getParameter("secretLevelValidity"), "");
        String name = Util.null2s(getParameter("name"), "");

        // qc 1491982 完整性校验所需参数，上传时计算文件签名等信息入库，下载时通过此参数进行计算校验文件是否被篡改
        String signInfo = "";
        String hashInfo = "";
        // 允许进行加密
        if (DocEncryptUtil.allowEncrypt(originalfilename)) {
            // 加密成功时再对文件签名且标识为已加密，否则signInfo为空，下载时不进行解密
            String[] signData = DocEncryptUtil.signFile(filerealpath);
            if (signData != null) {
                hashInfo = signData[0];
                signInfo = signData[1];
            }
        }
        String sql = "update imagefile set isaesencrypt=" + isaesencrypt + ", aescode='" + aescode + "',signinfo='"+signInfo+ "',hashinfo='"+hashInfo+ "',TokenKey='" + tokenKey + "',secretLevel=" + secretLevel + ",secretValidity='" + secretValidity + "' where imagefileid=" + imageid;
//        rs.writeLog("fileupload2222-----fileid:"+imageid+"---name:"+name+"--secretLevel:"+secretLevel+"--secretValidity:"+secretValidity+"--sql:"+sql);
        // 更新加密信息
        rs.executeUpdate(sql);

        /**上传解密 预留接口  start*/
        FileManager.upload(request,imageid,filerealpath,aescode,tokenKey,iszip,isaesencrypt,mpdata);
        /**上传解密 预留接口  end*/

        aliOSSObjectManager.uploadFile(filerealpath, originalfilename, iszip, isaesencrypt, aescode);

        try {                                        // add by liuyu to get image file width and height
            if (contenttype.indexOf("image") != -1 && needimagewidth) {
                File thefile = mpdata.getFile(uploadname);
                long fileLength = thefile.length();
                filesizes.add("" + fileLength);

                if (needzip) {
                    ZipInputStream zin = new ZipInputStream(new FileInputStream(thefile));
                    if (zin.getNextEntry() != null) source = new BufferedInputStream(zin);
                } else source = new BufferedInputStream(new FileInputStream(thefile));

                if (isaesencrypt.equals("1")) {
                    source = AESCoder.decrypt(source, aescode);
                }
                //byte[] imagebyte = new byte[64*1024] ;
                //StringBuffer buf = new StringBuffer();
                //while (source.read(imagebyte, 0, imagebyte.length) != -1) buf.append(imagebyte);
                //ByteArraySeekableStream bs =  new ByteArraySeekableStream((buf.toString()).getBytes()) ;
                //RenderedOp bimage = JAI.create("stream", bs);
                //imagewidth.add(""+bimage.getWidth()) ;
                //imageheight.add(""+bimage.getHeight()) ;


                ImageInfo ii = new ImageInfo();
                ii.setInput(source);
                if (!ii.check()) {
                    imagewidth.add("0");
                    imageheight.add("0");
                } else {
                    imagewidth.add("" + ii.getWidth());
                    imageheight.add("" + ii.getHeight());
                }
            } else {
                imagewidth.add("0");
                imageheight.add("0");
                filesizes.add("0");
            }
        } catch (Exception imgex) {
            imagewidth.add("0");
            imageheight.add("0");
            filesizes.add("0");
        }

        return imageid + "";
    }


    private boolean isMultipartData(HttpServletRequest req) {
        return Util.null2String(req.getContentType()).toLowerCase().startsWith("multipart/form-data");
    }


    public static String getCreateDir(String createdir) {

        if (createdir == null) {
            StaticObj staticObj = StaticObj.getInstance();
            staticObj.removeObject("SystemInfo");
            SystemComInfo syscominfo = new SystemComInfo();
            createdir = syscominfo.getFilesystem();
        }


        if (createdir == null || createdir.equals("")) createdir = GCONST.getSysFilePath();
        else {
            createdir = Util.StringReplace(createdir, "\\", "#$^123");
            createdir = Util.StringReplace(createdir, "/", "#$^123");
            createdir = Util.StringReplace(createdir, "#$^123", File.separator);
            //if( createdir.lastIndexOf(File.separator) < 0 ) createdir += File.separator ;

            if (!createdir.endsWith(File.separator)) {
                createdir += File.separator;
            }
        }

        Calendar today = Calendar.getInstance();
        String currentyear = Util.add0(today.get(Calendar.YEAR), 4);
        String currentmonth = Util.add0(today.get(Calendar.MONTH) + 1, 2);

        Random random = new Random();
        int randomint = 1 + random.nextInt(26);
        String charstr = Util.getCharString(randomint);

        createdir += currentyear + currentmonth + File.separatorChar + charstr + File.separatorChar;
        String ostype = System.getProperty("os.arch");
        String osname = System.getProperty("os.name").toLowerCase();
        //if (!ostype.equals("x86")&&!ostype.equals("amd64")) {
        if (!osname.startsWith("windows")) {
            try {
                if (!createdir.substring(0, 1).equals(File.separator)) {
                    new BaseBean().writeLog("WRAN................File path=[" + createdir + "]   os=[" + ostype + "]");
                    createdir = File.separator + createdir;
                    new BaseBean().writeLog("WRAN................Changed path=[" + createdir + "]   os=[" + ostype + "]");
                }
            } catch (Exception e) {
            }
        }
        return createdir;
    }

    //获取文件的原始名称
    public String getFileOriginalFileName(String uploadame) {
        return SecurityMethodUtil.textXssClean(mpdata.getOriginalFileName(uploadame));
    }

    //根据文件名获取文件
    public File getFile(String filename) {
        return mpdata.getFile(filename);
    }

    /**
     * 根据参数名判断文件是否可以上传, 查询参数名为file对应的文件的后缀
     *
     * @return
     */
    public boolean canUploadCheckBySecid() {
        return canUploadCheckBySecid("file");
    }

    /**
     * 根据 uplaodfilename 查找指定的物理文件的文件名，判断文件是否可以上传
     *
     * @param uplaodfilename
     * @return
     */
    public boolean canUploadCheckBySecid(String uplaodfilename) {
        int secid = Util.getIntValue(this.getParameter("secid"), -1);

        String paramFileName = this.getFileOriginalFileName(uplaodfilename); //获取参数当中的文件名
        if (secid <= 0) {
            return FileSuffixCheckUtil.isEnableUpload(paramFileName);
        } else {
            return FileSuffixCheckUtil.isEnableUploadBySecId(secid, paramFileName);
        }
    }


    /**
     * @param fileName 文件名
     * @param fieldId 附件字段id
     * @param workflowId 流程id
     * @return 文件上传后端校验是否通过
     */
    public boolean checkFileSuffix(String fileName, int fieldId, int workflowId) {
        RecordSet rs1 = new RecordSet();
        //查询单个字段设置
        String sql1 = "select limittype,limitvalue from workflow_fileupload where fieldid = " + fieldId + " and workflowid =" + workflowId;
        //查询默认设置
        String sql2 = "select limitvalue from workflow_base where id = " + workflowId;
        //查询新表单
        String sql3 = "select FIELDHTMLTYPE,TYPE from workflow_billfield where id = " + fieldId;
        //查询老表单主表
        String sql4 = "select fieldhtmltype,type from workflow_formdict where id = "+fieldId;
        //查询老表单明细表
        String sql5 = "select fieldhtmltype,type from workflow_formdictdetail where id = "+fieldId;

        String limitType = "";//单个字段设置类型
        String limitValueField = "";//单个字段设置限制格式
        String limitValueDefult = "";//默认限制格式
        String fileNameSuffix = FileSuffixCheckUtil.getFileNameSuffix(fileName);//上传的文件名后缀

        rs1.executeQuery(sql1);
        while (rs1.next()){
            limitType = Util.null2String(rs1.getString("limittype"));
            limitValueField = Util.null2String(rs1.getString("limitvalue"));
        }
        rs1.execute(sql2);
        while (rs1.next()){
            limitValueDefult = Util.null2String(rs1.getString("limitvalue"));
        }

        //新表单
        String fieldHtmlTypeNew = "";//新表单字段大类型
        String typeNew = "";//新表单字段小类型
        rs1.executeQuery(sql3);
        while (rs1.next()){
            fieldHtmlTypeNew = Util.null2String(rs1.getString(1));
            typeNew = Util.null2String(rs1.getString(2));
        }
        //校验新表单字段是否为图片，如果是图片，则不校验，直接放行
        if ("6".equals(fieldHtmlTypeNew) && "2".equals(typeNew)) {
            return true;
        }


        //老表单主表
        String fieldHtmlTypeOld = "";//老表单字段大类型
        String typeOld = "";//老表单字段小类型
        rs1.execute(sql4);
        while (rs1.next()){
            fieldHtmlTypeOld = Util.null2String(rs1.getString(1));
            typeOld = Util.null2String(rs1.getString(2));
        }
        //校验老表单字段是否为图片，如果是图片，则不校验，直接放行
        if ("6".equals(fieldHtmlTypeOld) && "2".equals(typeOld)) {
            return true;
        }

        //老表单明细表
        String fieldHtmlTypeDetailOld = "";//老表单明细字段大类型
        String typeDetailOld = "";//老表单明细字段小类型
        rs1.execute(sql5);
        while (rs1.next()){
            fieldHtmlTypeDetailOld = Util.null2String(rs1.getString(1));
            typeDetailOld = Util.null2String(rs1.getString(2));
        }
        //校验老表单字段是否为图片，如果是图片，则不校验，直接放行
        if ("6".equals(fieldHtmlTypeDetailOld) && "2".equals(typeDetailOld)) {
            return true;
        }

        //如果默认和字段都没有设置格式限制，或者字段选择了格式显示，但是格式限制为空，则直接允许提交
        if (("".equals(limitValueField) && "".equals(limitValueDefult)) || ("1".equals(limitType) && "".equals(limitValueField)) || limitValueDefult.indexOf(fileNameSuffix) != -1) {
            return true;
        }

        //先校验单个字段，如果单个字段没有设置格式校验，则校验默认的格式设置
        if ("1".equals(limitType)) {
            String[] formats = limitValueField.toLowerCase().split(",");
            return Util.contains(formats,fileNameSuffix.toLowerCase());
        }
        if ("0".equals(limitType) || "".equals(limitType)) {
            String[] restrictedFormat = limitValueDefult.toLowerCase().split(",");
            return Util.contains(restrictedFormat,fileNameSuffix.toLowerCase());
        }

        return false;
    }
    public static long getLongValue(String v, int def) {
        try {
            return Long.parseLong(v);
        } catch (Exception ex) {
            return def;
        }
    }

}

