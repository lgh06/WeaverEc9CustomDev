package com.customization.proxy.impl;


import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;
import weaver.file.Prop;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.rsa.security.RSA;
import weaver.sm.SM4Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WeaIocReplaceComponent("")
public class ProxygetAccFileConvert {


    @WeaReplaceAfter(value = "/api/doc/acc/getAccFileConvert", order = 1)
    public String getAccFileConvert(WeaAfterReplaceParam weaAfterReplaceParam) {
        String loginid = Util.null2String(weaAfterReplaceParam.getParamMap().get("loginid"));
        //设置找回密码是的loginid到登录名
        weaAfterReplaceParam.getRequest().getSession(true).setAttribute("weaver_forgetLoginId", loginid);
        String data = weaAfterReplaceParam.getData();
        String path="https://file.keking.cn/onlinePreview?url=aHR0cHM6Ly9maWxlLmtla2luZy5jbi9kZW1vL%2BWPjOaOp%2Bezu%2Be7n%2BaMh%2Bagh%2Be7n%2BiuoUAyMDIxLTEyLTA5LmRvY3g%3D";
        data="{\"desc\":0,\"result\":0,\"data\":[{\"path\":\""+path+"\"}],\"useTime\":50,\"convert\":\"client\",\"fileid\":7456}";
        return data;
    }
    @WeaReplaceAfter(value = "/api/doc/acc/convertFileForMobile", order = 1)
    public String afterSaveLoginId(WeaAfterReplaceParam weaAfterReplaceParam) {
        String loginid = Util.null2String(weaAfterReplaceParam.getParamMap().get("loginid"));
        //设置找回密码是的loginid到登录名
        weaAfterReplaceParam.getRequest().getSession(true).setAttribute("weaver_forgetLoginId", loginid);
        String data = weaAfterReplaceParam.getData();
        String path="https://file.keking.cn/onlinePreview?url=aHR0cHM6Ly9maWxlLmtla2luZy5jbi9kZW1vL%2BWPjOaOp%2Bezu%2Be7n%2BaMh%2Bagh%2Be7n%2BiuoUAyMDIxLTEyLTA5LmRvY3g%3D";
        data="{\"desc\":0,\"result\":0,\"data\":[{\"path\":\""+path+"\"}],\"useTime\":541,\"convert\":\"client\"}";
        return data;
    }



}
