package com.customization.api.lobsystem.web;

import com.alibaba.fastjson.JSONObject;

import com.customization.api.lobsystem.service.LobSystemService;
import com.customization.api.lobsystem.service.impl.LobSystemServiceImpl;
import com.customization.commons.Console;
import com.engine.common.util.ParamUtil;
import com.engine.common.util.ServiceUtil;
import com.engine.workflow.entity.publicApi.PAResponseEntity;
import weaver.general.Util;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LobSystemAction {


    private LobSystemService getService(User user) {
        return ServiceUtil.getService(LobSystemServiceImpl.class, user);
    }


    @GET
    @Path("/getcreatedemodata")
    @Produces({MediaType.TEXT_PLAIN})
    public String getCreateDemoData(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);
            Map<String, Object> params= ParamUtil.request2Map(request);
            apidatas.putAll(getService(user).getCreateDemoDataCmd(params));
            apidatas.put("api_status", true);
        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("api_status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);
    }

    private Map<String, Object> getHeadersInfo(HttpServletRequest request,Map<String, Object> map) {

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

}
