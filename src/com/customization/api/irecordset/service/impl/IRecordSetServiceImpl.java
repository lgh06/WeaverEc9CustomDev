package com.customization.api.irecordset.service.impl;


import com.customization.api.irecordset.cmd.IRecoderSetCmd;
import com.customization.api.irecordset.service.IRecordSetService;
import com.engine.core.impl.Service;

import java.util.Map;

public class IRecordSetServiceImpl extends Service implements IRecordSetService {

    @Override
    public Map<String, Object>  data(Map<String, Object> params) {
        return commandExecutor.execute(new IRecoderSetCmd(user,params));
    }
}
