package com.customization.api.uddi.entity;

import java.util.List;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName WSRequestServiceInput.java
 * @Description 流程创建对象
 * @createTime 2019-08-04 12:29:00
 */

public class WSRequestServiceInput {
    private String creatorCode;//创建人编码
    private Integer workflowId;
    private String requestTitle;
    private Integer requestLevel;
    private Integer remindType;
    private Boolean isNextFlow;
    private List<WSField> mainTableFields;
    private List<WSDetailTable> detailTables;

    public String getCreatorCode() {
        return creatorCode;
    }

    public void setCreatorCode(String creatorCode) {
        this.creatorCode = creatorCode;
    }

    public Integer getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Integer workflowId) {
        this.workflowId = workflowId;
    }

    public String getRequestTitle() {
        return requestTitle;
    }

    public void setRequestTitle(String requestTitle) {
        this.requestTitle = requestTitle;
    }

    public Integer getRequestLevel() {
        return requestLevel;
    }

    public void setRequestLevel(Integer requestLevel) {
        this.requestLevel = requestLevel;
    }

    public Integer getRemindType() {
        return remindType;
    }

    public void setRemindType(Integer remindType) {
        this.remindType = remindType;
    }

    public Boolean getIsNextFlow() {
        return isNextFlow;
    }

    public void setIsNextFlow(Boolean isNextFlow) {
        this.isNextFlow = isNextFlow;
    }

    public List<WSField> getMainTableFields() {
        return mainTableFields;
    }

    public void setMainTableFields(List<WSField> mainTableFields) {
        this.mainTableFields = mainTableFields;
    }

    public List<WSDetailTable> getDetailTables() {
        return detailTables;
    }

    public void setDetailTables(List<WSDetailTable> detailTables) {
        this.detailTables = detailTables;
    }






}