package com.customization.api.irecordset.service;


import java.util.Map;

public interface IRecordSetService {

    Map<String, Object>   data(Map<String, Object> params);

}
