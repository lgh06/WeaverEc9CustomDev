package com.customization.proxy.impl;


import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import weaver.conn.RecordSet;


import java.util.HashMap;
import java.util.Map;

@WeaIocReplaceComponent("NewRequestServiceImpl")
public class ProxyNewRequestServiceImpl {


    //流程发起界面增加流程描述的内容返回
    @WeaReplaceAfter(value = "/api/workflow/createreq/wfinfo",order = 1)
    public String afterOrg(WeaAfterReplaceParam weaAfterReplaceParam){

        RecordSet rs= new RecordSet();
       
        rs.execute("select id,workflowdesc from workflow_base ");
        Map<String ,String > descMap=new HashMap<>();
        while (rs.next()){
            descMap.put(rs.getString("id"),rs.getString("workflowdesc"));
        }


        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文

        JSONObject json = JSONObject.fromObject(data);
        json.put("customDesc","ProxyNewRequestServiceImpl 202008261127");
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                int wfbeansSize=json.getJSONArray("datas").getJSONObject(i).getJSONArray("wfbeans").size();
                for (int j = 0; j <wfbeansSize; j++) {
                    String wfDesc="";
                    String wfid=json.getJSONArray("datas")
                            .getJSONObject(i)
                            .getJSONArray("wfbeans")
                            .getJSONObject(j).get("id").toString();
                    wfDesc=descMap.get(wfid);
                    json.getJSONArray("datas").getJSONObject(i).getJSONArray("wfbeans").getJSONObject(j)
                            .put("wfdesc",wfDesc);
                }
            }
        }


        return json.toString();
    }
}
