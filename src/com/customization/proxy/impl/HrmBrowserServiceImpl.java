package com.customization.proxy.impl;


import com.customization.commons.Console;
import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WeaIocReplaceComponent("ResourceBrowserService")
public class HrmBrowserServiceImpl {


    //人力资源字段返回值展现调整
    @WeaReplaceAfter(value = "/api/public/browser/complete/1",order = 1)
    public String after(WeaAfterReplaceParam weaAfterReplaceParam){
        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
        JSONObject json = JSONObject.fromObject(data);
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                json.getJSONArray("datas").getJSONObject(i).put("lastname",
                        json.getJSONArray("datas").getJSONObject(i).get("title"));
            }
        }
        return json.toString();
    }





    //多展开一层组织结构
    @WeaReplaceAfter(value = "/api/public/browser/data/1",order = 1)
    public String afterOrg(WeaAfterReplaceParam weaAfterReplaceParam){
        Console.log("afterOrg");
        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文

        String childStr="[{\"children\":[],\"icon\":\"/images/treeimages/subCopany_Colse_wev8.gif\",\"id\":\"200\",\"isParent\":\"true\",\"lastname\":\"销售部\",\"nocheck\":\"Y\",\"nodeid\":\"dept_200x\",\"type\":\"dept\"},{\"children\":[],\"icon\":\"/images/treeimages/subCopany_Colse_wev8.gif\",\"id\":\"199\",\"isParent\":\"true\",\"lastname\":\"总经理室\",\"nocheck\":\"Y\",\"nodeid\":\"dept_199x\",\"type\":\"dept\"},{\"children\":[],\"icon\":\"/images/treeimages/subCopany_Colse_wev8.gif\",\"id\":\"201\",\"isParent\":\"true\",\"lastname\":\"项目部\",\"nocheck\":\"Y\",\"nodeid\":\"dept_201x\",\"type\":\"dept\"}]";
        JSONArray childJson= JSONArray.fromObject(childStr);

        JSONObject json = JSONObject.fromObject(data);
        if(json.containsKey("datas")){
            JSONArray jsonArray=json.getJSONArray("datas");
            for (int i = 0; i <jsonArray.size() ; i++) {
                json.getJSONArray("datas").getJSONObject(i).put("children",childJson);
            }
        }


        return json.toString();
    }


}
