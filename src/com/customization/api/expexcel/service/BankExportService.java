package com.customization.api.expexcel.service;

import java.util.Map;

public interface BankExportService {

    /**
     * 获取form表单
     * @param params
     * @return
     */
    Map<String, Object> getweatable_Orgin(Map<String, Object> params);

}
