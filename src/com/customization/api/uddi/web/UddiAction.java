package com.customization.api.uddi.web;


import com.customization.api.uddi.service.UddiService;
import com.customization.api.uddi.service.impl.UddiServiceImpl;
import com.engine.common.util.ParamUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Map;


import com.engine.common.util.ServiceUtil;
import weaver.general.Util;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

public class UddiAction {

    private UddiService getService(User user) {
        return ServiceUtil.getService(UddiServiceImpl.class, user);
    }


    @GET
    @Path("/wsdl")
    @Produces({MediaType.TEXT_XML})
    public String getWsdlInfo(@Context HttpServletRequest request, @Context HttpServletResponse response){
        String result="-09";
        try {
            Map params=ParamUtil.request2Map(request);
            User user = new User(1);
            params.put("serverName" ,  request.getServerName());
            result=getService(user).getWsdl(params,user);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        return result;
    }


    /**
     * 创建流程
     * @param request
     * @param response
     * @return
     */
    @POST
    @Path("/createReq")
    @Produces({MediaType.TEXT_XML})
    public String create(@Context HttpServletRequest request, @Context HttpServletResponse response){
        User user = new User(1);
        return getService(user).createRequest(request,response);
    }
}
