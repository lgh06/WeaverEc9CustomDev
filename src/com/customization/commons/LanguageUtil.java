package com.customization.commons;

import net.sf.json.JSONObject;
import okhttp3.*;
import org.apache.commons.codec.digest.DigestUtils;
import weaver.conn.RecordSet;

import java.io.IOException;
import java.util.Random;

public class LanguageUtil {
    public static void Translate(String table,String column) {
        Translate( table, column,"zh" ,"en");
    }

    public static void Translate(String table,String column,String from ,String to) {

        String sql = " select "+column+" from "+table+" where "+column+" not like '~`~`%' GROUP BY   "+column;
        RecordSet rs = new RecordSet();
        RecordSet rs2 = new RecordSet();
        rs.executeQuery(sql);
        System.out.println("待翻译数量："+rs.getCounts());
        int i =0;
        while (rs.next()) {
            String q=rs.getString(column);

            String appid="20200716000520571";
            String secrit="sChYLJ9wvkqAmVBFwvbx";
            String salt=""+new Random().nextInt(1000);

            String sign= DigestUtils.md5Hex(appid+q+salt+secrit);
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "");
            Request request = new Request.Builder()
                    .url("http://api.fanyi.baidu.com/api/trans/vip/translate?q="+q+"&from=zh&to=en&appid="+appid+"&salt="+salt+"&sign="+sign)
                    .method("POST", body)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                String res=response.body().string();
                JSONObject resjson=JSONObject.fromObject(res);
                if(resjson.containsKey("trans_result")){
                    String dst=resjson.getJSONArray("trans_result").getJSONObject(0).getString("dst");
                    System.out.println("("+i+"/"+rs.getCounts()+")from:"+q+";"+"dst:"+dst);
                    String toLanguage=("~`~`7 "+q+"`~`8 "+dst+"`~`~").replace("'","''");
                    String updSql="update "+table+ "  set "+column+"='"+toLanguage+"' where "+column+"='"+q+"'";
                   // System.out.println(updSql);
                    rs2.executeUpdate(updSql);
                }else{
                    System.out.println(resjson.toString());
                    break;
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ++i;

        }
    }

    public static void TranslateHtmlLabelInfo(int fromlanguageid ,int tolanguageid) {

        String sql = "select * from HtmlLabelInfo  h where Languageid="+fromlanguageid+" and indexId<0   and " +
                "(  h.labelName =  (select labelName from HtmlLabelInfo t  where  t.indexId=h.indexId and t.Languageid="+tolanguageid+") or " +
                " not EXISTS (select labelName from HtmlLabelInfo t  where  t.indexId=h.indexId and t.Languageid=8 )" +
                " )";
        RecordSet rs = new RecordSet();
        RecordSet rs2 = new RecordSet();
        rs.executeQuery(sql);
        System.out.println("待翻译数量："+rs.getCounts());
        int i =0;
        while (rs.next()) {
            String labelname=rs.getString("labelname");
            String indexid=rs.getString("indexid");

            String appid="20200716000520571";
            String secrit="sChYLJ9wvkqAmVBFwvbx";
            String salt=""+new Random().nextInt(1000);

            String sign= DigestUtils.md5Hex(appid+labelname+salt+secrit);
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = RequestBody.create(mediaType, "");
            Request request = new Request.Builder()
                    .url("http://api.fanyi.baidu.com/api/trans/vip/translate?q="+labelname+"&from=zh&to=en&appid="+appid+"&salt="+salt+"&sign="+sign)
                    .method("POST", body)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                String res=response.body().string();
                JSONObject resjson=JSONObject.fromObject(res);
                if(resjson.containsKey("trans_result")){
                    String dst=resjson.getJSONArray("trans_result").getJSONObject(0).getString("dst");
                    System.out.println("("+i+"/"+rs.getCounts()+")from:"+labelname+";"+"dst:"+dst);
                  //  String toLanguage=("~`~`7 "+labelname+"`~`8 "+dst+"`~`~").replace("'","''");

                    String selSql="select * from HtmlLabelInfo where  languageid='"+tolanguageid+"' and indexid='"+indexid+"'";
                    rs2.execute(selSql);
                    boolean isExits=rs2.getCounts()>0?true:false;


                   //System.out.println(updSql);
                    if(isExits){
                        String updSql="update  HtmlLabelInfo  set labelname='"+dst+"' where languageid='"+tolanguageid+"' and indexid='"+indexid+"'";
                        rs2.executeUpdate(updSql);
                    }else{
                        String insertSql=" insert into HtmlLabelInfo (labelname,languageid,indexid) values('"+dst+"',"+tolanguageid+","+indexid+")";
                       // System.out.println(insertSql);
                        rs2.execute(insertSql);
                    }
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ++i;

        }
    }
}
