package com.customization.proxy.impl;



import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;
import weaver.hrm.User;

import java.util.HashMap;
import java.util.Map;

@WeaIocReplaceComponent("RequestLogServiceImpl")
public class ProxyRequestLogServiceImpl {


    //
    @WeaReplaceAfter(value = "/api/workflow/reqform/getRequestLogBaseInfo",order = 1)
    public String afterExceute(WeaAfterReplaceParam weaAfterReplaceParam){

        RecordSet rs= new RecordSet();


        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
        JSONObject json = JSONObject.fromObject(data);
        json.put("customDesc","ProxyRequestLogServiceImpl 20200909 lth");
        if(json.containsKey("allrequestInfos")){
            JSONArray jsonArray=json.getJSONArray("allrequestInfos");//获取所有关联的流程的流程信息

            for (int i = 0; i <jsonArray.size() ; i++) {
                //获取每个关联流程的requestid
                int requestid=json.getJSONArray("allrequestInfos").getJSONObject(i).getInt("requestid");

                String sql="select requestname,creater,currentnodetype from workflow_requestbase  where requestid='"+requestid+"'";
                rs.execute(sql);
                if(rs.first()){
                        String reqeustnamePrefix = (new User(rs.getInt("creater"))).getLastname();
                        int  currentnodetype = rs.getInt("currentnodetype");
                        if (currentnodetype == 3) {
                            reqeustnamePrefix = reqeustnamePrefix + "-已归档-";
                        } else {
                            reqeustnamePrefix = reqeustnamePrefix + "-审批中-";
                        }
                        String newRequestName=reqeustnamePrefix+rs.getString("requestname");
                        String old_requestname =json.getJSONArray("allrequestInfos").getJSONObject(i).getString("requestname");
                        json.getJSONArray("allrequestInfos").getJSONObject(i).put("old-requestname",old_requestname);

                        json.getJSONArray("allrequestInfos").getJSONObject(i).put("requestname",newRequestName);
                        String newSignshowname=json.getJSONArray("allrequestInfos").getJSONObject(i).getString("signshowname").replace(old_requestname,newRequestName);
                        json.getJSONArray("allrequestInfos").getJSONObject(i).put("signshowname",newSignshowname);
                }

            }
        }


        return json.toString();
    }
}
