package com.customization.api.uddi.entity;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName WSFile.java
 * @Description 文件
 * @createTime 2019-08-04 12:53:00
 */
public  class WSFile {
    private String name;//文件名称
    private String url;//链接

    public WSFile() {}

    public WSFile(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
