package com.customization.test;



import com.api.cube.web.CubeTreeAction;
import com.api.doc.detail.util.DocDownloadCheckUtil;
import com.api.doc.detail.web.DocAcction;
import com.api.formmode.web.FormmodeFormAction;
import com.api.login.web.LoginAction;
import com.api.mobilemode.web.mobile.BaseMobileAction;
import com.customization.api.uddi.cmd.CreateRequestCmd;

import com.customization.commons.Console;


import com.engine.cube.web.ModeAppAction;
import com.engine.hrm.web.ResourceFieldDefinedAction;
import com.engine.kq.biz.KQScheduleSignImportJob;
import net.sf.json.JSONObject;
import okhttp3.*;
import org.junit.Test;
import sun.misc.BASE64Encoder;

import weaver.conn.RecordSet;


import weaver.fna.maintenance.FnaAmountControl;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.hrm.User;

import weaver.hrm.resource.ResourceComInfo;

import weaver.workflow.action.ESBAction;
import weaver.workflow.webservices.WorkflowRequestInfo;
import weaver.workflow.webservices.WorkflowServiceImpl;




import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


public class TestCmd extends BaseTest{

    @Test
    public void tsetESB() {
        // new com.weaver.esb.util.wsdl.WsdlUtil();
        String str = "getUserId.Response>";
        str = str.replace(".Response>", "__Response>");
        str = "2022-06-14 07:52";

        System.out.println(str.substring(0, 10));
        //Soap11HttpBindingImporter

        System.out.println(DocDownloadCheckUtil.DncodeFileid("6a340441282d4c1d"));

        //  com.api.doc.detail.web


    }
    @Test
    public void tsetOkHttp() {
       // KQScheduleSignImportJob
        //FnaAmountControl
        String kbtime="2022-06-16 09:00";
        try {
            Date date=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(kbtime);
            Calendar calendar = Calendar.getInstance();
            /* HOUR_OF_DAY 指示一天中的小时 */
            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, 1);
            if((new Date()).after(calendar.getTime())){

            }

            System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(calendar.getTime()));

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }




        System.out.println("getSeparator:"+ weaver.general.Util.getSeparator());
     /*   System.out.println(com.api.doc.detail.util.DocDownloadCheckUtil.DncodeFileid("2614ca31d9084be598407aaad170657e"));

        //weaver.wps.view.linux.convert.PreviewFile
       // com.engine.board.web.WidgetAction
        //FormmodeFormAction
       // ModeAppAction

        int formmodeid=42;
        int billid=1268;
        weaver.formmode.setup.ModeRightInfo ModeRightInfo = new weaver.formmode.setup.ModeRightInfo();
        ModeRightInfo.rebuildModeDataShareByEdit(1,formmodeid,billid);
*/
    }


    @Test
    public void testWorkflowService() {
    WorkflowServiceImpl workflowService = new WorkflowServiceImpl();
        String  [] sqlwhere={"t1.requestid=3242738"};
        WorkflowRequestInfo [] res= workflowService.getToDoWorkflowRequestList(1,10000,10000,13513,sqlwhere);
        System.out.println("123:"+res.length);

        res= workflowService.getProcessedWorkflowRequestList(1,100,10000,13513,sqlwhere);
        for (WorkflowRequestInfo wf :
                res) {
            System.out.println(wf.getRequestId());

        }
        System.out.println("归档流程:"+res.length);


    }

    @Test
    public void fixPayWorkflow() {
        boolean isexitorder=false;
        int requestid=0;
        RecordSet rst=new RecordSet();
        RecordSet rs=new RecordSet();

        rst.execute("select *  from uf_cbs_pay where   LINENR='01' ");
        while(rst.next()) {

            Console.log("---");

            requestid=rst.getInt("wfid");

            int wfid = 0;
            rs.execute(" select * from workflow_requestbase where requestid='" + requestid + "'");

            String CURRENTNODETYPE = "";
            if (rs.next()) {
                requestid = rs.getInt("requestid");
                wfid = rs.getInt("workflowid");
                CURRENTNODETYPE = rs.getString("CURRENTNODETYPE");
            }
            //获取流程上面的节点操作者ID
            System.out.println("requestid:"+requestid);
            rs.execute("select RECEIVEDPERSONIDS from  workflow_requestlog where requestid='" + requestid + "' order by operatedate||' '||OPERATETIME desc");

            int userid = 0;
            if (rs.next()) {
                userid = rs.getInt("RECEIVEDPERSONIDS");
            }

            //2021/2/8 10:14 lth     改为使用固定CBS账号(ID13513)提交，避免流程强制干预等状态下通过回写接口提交流程

            System.out.println("userid:  " + userid);
            System.out.println("CURRENTNODETYPE:  " + CURRENTNODETYPE);


            if (userid == 13513) {//是支付账号的待办
                isexitorder = true;

                String updateStatus = "update uf_cbs_pay set status='99' where wfid='" + requestid + "'";
                rs.execute(updateStatus);
                Console.log("是支付账号的待办:" + updateStatus);
            } else {//如果流程已经归档直接更新数据为已处理
                isexitorder = false;
                String updateStatus = "update uf_cbs_pay set status='"+CURRENTNODETYPE+"' where wfid='" + requestid + "'";
                rs.execute(updateStatus);

                Console.log("流程已归档,并且操作-updateStatus:" + updateStatus);

                if(CURRENTNODETYPE.equals("3")){//已经归档的数据
                    updateStatus = "update uf_fp set gllczt='1' where zylc='" + requestid + "'";
                    rs.execute(updateStatus);
                }



            }
        }





    }


    public static void main(String[] args) {
        String  sqls="select sum(se) as se from( " +
                "select  sum(nvl(sk,0)) as se from formtable_main_639_dt1 dt, formtable_main_639 fm where fm.id=dt.mainid and fm.requestid=3105414  and sl=1 " +
                "union all " +
                "select  sum(nvl(se,0)) as se from formtable_main_639_dt3 dt,formtable_main_639 fm where fm.id=dt.mainid and fm.requestid=3105414 and sl=1 " +
                "union all " +
                "select  sum(nvl(se,0)) as se from formtable_main_639_dt5 dt,formtable_main_639 fm where fm.id=dt.mainid and fm.requestid=3105414  and sl=1 " +
                ") t";
        
        JSONObject jsonObject =new JSONObject();
        jsonObject.put("SOURCE_SYS","");
        jsonObject.put("DOCUMENT_NUM","");
        jsonObject.put("APPL_DATE","");
        System.out.println(jsonObject.toString());
    }

    @Test
    public void testPdf2Txt() throws Exception {

      //System.out.println("docid："+com.api.doc.detail.util.DocDownloadCheckUtil.EncodeFileid("1191731",new User(1)));

        User user=new User(1);


        JSONObject json=new  JSONObject();
        json.put("status","-1");
        ResourceComInfo rci=new ResourceComInfo();
        String workcode= rci.getWorkcode(""+user.getUID());
        workcode="lga0";//登陆人的账号

        BaseBean baseBean=new BaseBean();
        String authorization="";
        String url= Util.null2String(baseBean.getPropValue("angel_fna","image_url"));
        if(url.equals("")){
            url="https://vpn.ecsphere.net:8443";
        }
        String clientname= Util.null2String(baseBean.getPropValue("angel_fna","image_clientname"));
        if(clientname.equals("")){
            clientname="oa";
        }

        String secrit= Util.null2String(baseBean.getPropValue("angel_fna","image_secrit"));
        if(secrit.equals("")){
            secrit="123456";
        }
        final BASE64Encoder encoder = new BASE64Encoder();
        final String text = clientname+":"+secrit;
        final byte[] textByte = text.getBytes("UTF-8");
        authorization = encoder.encode(textByte);
        System.out.println("authorization:"+authorization);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url+"/angel-auth/oauth/token?grant_type=password&username="+workcode+"&password="+UUID.randomUUID())
                .method("GET", null)
                .addHeader("Authorization", "Basic "+authorization)
                .build();
        Response response = client.newCall(request).execute();
        if(response.isSuccessful()){
            JSONObject res=JSONObject.fromObject(response.body().string());
            if(res.containsKey("access_token")){
                String access_token=res.getString("access_token");
                json.put("access_token","access_token");
                json.put("status","0");
                json.put("redirect_uri",url+"/angel/#/file/list?token="+access_token);
            }else{
                json.put("msg",res);
                Console.log(request.body()+":"+res.toString());
            }
        }else{
            json.put("msg",response.body().string());
        }

        System.out.println(json.toString());


    }

    /**
     * 是否检测键盘按键横向连续
     */
    public static String CHECK_HORIZONTAL_KEY_SEQUENTIAL = "Y";
    /**
     * 键盘物理位置横向不允许最小的连续个数
     */
    public static String LIMIT_HORIZONTAL_NUM_KEY = "4";

    /**
     * 是否检测键盘按键斜向连续
     */
    public static String CHECK_SLOPE_KEY_SEQUENTIAL = "Y";
    /**
     * 键盘物理位置斜向不允许最小的连续个数
     */
    public static String LIMIT_SLOPE_NUM_KEY = "4";



    /**
     * 是否检测连续字符相同
     */
    public static String CHECK_SEQUENTIAL_CHAR_SAME = "Y";
    /**
     * 密码口令中相同字符不允许最小的连续个数
     */
    public static String LIMIT_NUM_SAME_CHAR = "4";
    /**
     * 是否检测包含登录名
     */
    public static String CHECK_HAS_LOGINID = "Y";

    /**
     * 密码口令中不允许最小包含登录名连续的个数
     */
    public static String LIMIT_NUM_LOGINID = "4";


    public static String CHECK_HAS_BIRTHDAY = "Y";
    public static String LIMIT_NUM_BIRTHDAY = "4";

    public static String CHECK_HAS_MOBILE = "Y";
    public static String LIMIT_NUM_MOBILE = "4";

    public static String CHECK_HAS_MOBILECALL = "Y";
    public static String LIMIT_NUM_MOBILECALL = "4";

    public static String CHECK_HAS_TELEPHONE = "Y";
    public static String LIMIT_NUM_TELEPHONE = "4";

    public static String CHECK_HAS_CERTIFICATENUM = "Y";
    public static String LIMIT_NUM_CERTIFICATENUM = "4";

    public static String CHECK_HAS_WORKCODE = "Y";
    public static String LIMIT_NUM_WORKCODE = "4";






    @Test
    public void requestForward() {
        String password="05200521";
        String loginid="lb";

    }

    public static boolean checkContainWords(String password,String keys,int num){
        boolean res=false;
        int length =keys.length();
        if(length>=num) {
            for (int i = 0; i <=length-num ; i++) {

                System.out.println(""+keys.substring(i,i+num).toLowerCase());
                if (password.toLowerCase().contains(keys.substring(i,i+num).toLowerCase())) {
                    res=true;
                    break;
                }
            }
        }
        return res;
    }

    public static String add(String value1, String value2) {
        BigDecimal b1 = new BigDecimal(value1);
        BigDecimal b2 = new BigDecimal(value2);
        BigDecimal result = b1.add(b2);

        return result.toString();
    }



    @Test
    public void test_AddSlabel() throws Exception {
        int dtno = Util.getIntValue("1",-1);
        if(dtno>-1){
            Console.log("传递了明细号 针对明细号进行导入");
            String  var44="formtable_main_324";
            var44=var44.substring(0,var44.lastIndexOf("_")+1)+dtno;
            System.out.println("var44:"+var44);
        }else{
            Console.log("传递了明细号 原导入逻辑");
        }
        /*
        SLDataInfo dataInfo = new SLDataInfo();   //声明密级属性对象
        dataInfo.setClassificationLevel("3"); //设置密级等级
        dataInfo.setClassifiedTime("20200728000000Z"); //设置定密时间
        dataInfo.setDeclassifiedTime("20210728000000Z"); //设置解密具体时间
        dataInfo.setDrafterName("黄纯"); //设置文件起草人姓名
        dataInfo.setDrafterID("111"); //设置文件起草人账号ID
        dataInfo.setIssuerName("郑青"); //设置文件签发人姓名
        dataInfo.setIssuerID("222"); //设置文件签发人账号ID
        dataInfo.setClassifiedOrganizationName("湖北省人民检察院"); //设置定密单位名称
        dataInfo.setClassifiedOrganizationID("1"); //设置定密单位编号
        dataInfo.setClassificationBasisDegree("3"); //设置定密依据密级等级


        String name="63f0ba0c-8b8c-47c6-b7db-66c84522bac5.doc";
       // ServiceUtil.adslAddSlabelEx("/Users/liutaihong/工作/客户/H-湖北省监察院/下载加密/人事管理调研提纲V2.0.docx",dataInfo.getXmlPath(),"/Users/liutaihong/工作/客户/H-湖北省监察院/下载加密/人事管理调研提纲V2.0加密.docx");

       ServiceUtil.adslRemoveSlabel("/Users/liutaihong/工作/客户/H-湖北省监察院/下载加密/test/资产条码打印 - 加密.docx","/Users/liutaihong/工作/客户/H-湖北省监察院/下载加密/test/资产条码打印.docx");
*/
    }



    @Test
    public void testPublicFunction() throws Exception {



        String xml="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ws=\"http://soap.ic1ng.cn/ws\">" +
                "   <soapenv:Header/>" +
                "   <soapenv:Body>" +
                "      <ws:createReq>" +
                "         <!--Zero or more repetitions:-->" +
                "         <row>" +
                "            <!--Optional:-->" +
                "            <KEY>key9887</KEY>" +
                "            <!--Optional:-->" +
                "            <CREATOR>1</CREATOR>" +
                "            <!--Optional:-->" +
                "            <REQUESTNAME>99999911121流程标题</REQUESTNAME>" +
                "            <!--Optional:-->" +
                "            <REQUESTLEVEL>1</REQUESTLEVEL>" +
                "            <MAINDATA>" +
                "               <!--Optional:-->" +
                "               <ZW>18</ZW>" +
                "               <!--Optional:-->" +
                "               <FJ>23</FJ>" +
                "               <!--Optional:-->" +
                "               <GWLX>0</GWLX>" +
                "               <!--Optional:-->" +
                "               <DRLZYZD>1</DRLZYZD>" +
                "               <!--Optional:-->" +
                "               <DXWB111>1</DXWB111>" +
                "            </MAINDATA>" +
                "            <DETAILDATA>" +
                "               <!--Optional:-->" +
                "               <FORMTABLE_MAIN_3_DT1>" +
                "                  <!--Zero or more repetitions:-->" +
                "                  <item>" +
                "                     <!--Optional:-->" +
                "                     <DXWB1>DXWB1</DXWB1>" +
                "                     <!--Optional:-->" +
                "                     <RLZY>2</RLZY>" +
                "                  </item>" +
                "               </FORMTABLE_MAIN_3_DT1>" +
                "               <!--Optional:-->" +
                "               <FORMTABLE_MAIN_3_DT2>" +
                "                  <!--Zero or more repetitions:-->" +
                "                  <item>" +
                "                     <!--Optional:-->" +
                "                     <ZS>9999</ZS>" +
                "                  </item>" +
                "               </FORMTABLE_MAIN_3_DT2>" +
                "            </DETAILDATA>" +
                "         </row>" +
                "      </ws:createReq>" +
                "   </soapenv:Body>" +
                "</soapenv:Envelope>";
        String workflowId="2";
        new CreateRequestCmd().doCreateByXml(xml,workflowId);






    }



}
