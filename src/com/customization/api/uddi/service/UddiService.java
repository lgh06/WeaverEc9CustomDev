package com.customization.api.uddi.service;

import weaver.hrm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface UddiService {


    /**
     * 生成WSDL文件
     * @param params
     * @param user
     * @return
     */
    String getWsdl(Map<String, Object> params, User user);

    /**
     * 创建流程
     * @param request
     * @param response
     * @return
     */
    String createRequest(HttpServletRequest request, HttpServletResponse response);

}
