package com.customization.proxy.impl;


import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;

import net.sf.json.JSONObject;
import weaver.conn.RecordSet;

import java.text.SimpleDateFormat;
import java.util.Date;


@WeaIocReplaceComponent("")
public class HrmResourcelistService {

        @WeaReplaceAfter(value = "/api/hrm/search/getHrmSearchResult",order = 1)
        public String afterExceute(WeaAfterReplaceParam weaAfterReplaceParam) {
            String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
            JSONObject obj = JSONObject.fromObject(data);
            obj.put("customDesc", "ProxyHrmResourceAddService 20220428 ");
            obj.put("sessionkey", "94250");
            return obj.toString();
        }


        @WeaReplaceAfter(value = "/api/hrm/organization/getResourceSearchList",order = 1)
        public String afterExceute2(WeaAfterReplaceParam weaAfterReplaceParam) {
            String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
            JSONObject obj = JSONObject.fromObject(data);
            obj.put("customDesc", "ProxyHrmResourceAddService 20220428");
            obj.put("sessionkey", "94250");
            return obj.toString();
        }


    @WeaReplaceAfter(value = "/api/hrm/matrix/pages/matrixList/addMatrixTable",order = 1)
    public String addMatrixTable(WeaAfterReplaceParam weaAfterReplaceParam) {

        String data = weaAfterReplaceParam.getData();//这个就是接口执行完的报文
            JSONObject obj = JSONObject.fromObject(data);
            obj.put("customDesc", "ProxyHrmResourceAddService 重置状态不是已办结的最新的100条数据的权限 20220515");
            new Thread((new Runnable() {

                @Override
                public void run() {
                RecordSet rs=new RecordSet();
                rs.execute(" update uf_yjfktz  set qxzgzt ='0' ");
                rs.execute(" select top 100 id,formmodeId from uf_yjfktz  where formmodeid is not null and zt<>3    order by id  desc ");
                while (rs.next()){
                    int formmodeid=rs.getInt("formmodeId");
                    int billid=rs.getInt("id");
                    weaver.formmode.setup.ModeRightInfo ModeRightInfo = new weaver.formmode.setup.ModeRightInfo();
                    ModeRightInfo.rebuildModeDataShareByEdit(1,formmodeid,billid);
                }
                }
            })).start();
            return obj.toString();
        }
    }
