package com.customization.api.expexcel.service.impl;

import com.customization.api.expexcel.cmd.BankExportCmd;
import com.customization.api.expexcel.service.BankExportService;
import com.engine.core.impl.Service;

import java.util.Map;

public class BankExportServiceImpl  extends Service implements BankExportService {

    @Override
    public Map<String, Object> getweatable_Orgin(Map<String, Object> params) {
        return commandExecutor.execute(new BankExportCmd(user,params));
    }
}
