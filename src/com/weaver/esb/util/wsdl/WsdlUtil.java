//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.weaver.esb.util.wsdl;

import com.alibaba.fastjson.JSONObject;
import com.api.integration.esb.bean.ParamBean;
import com.api.integration.esb.constant.EsbConstant;
import com.api.integration.util.ClassLoaderSwapper;
import com.api.integration.util.JavaUtil;
import com.api.integration.util.RandomUtil;
import com.customization.commons.Console;
import com.ibm.wsdl.ServiceImpl;
import com.weaver.esb.server.enums.DataType;
import com.weaver.esb.server.enums.ParamType;
import com.weaver.esb.server.enums.TransmitType;
import com.weaver.esb.server.impl.auth.SignatureUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLClassLoader;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Port;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.ExtensibilityElement;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.wsdl.extensions.soap12.SOAP12Address;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import javax.xml.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.dom4j.Attribute;
import org.dom4j.Comment;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import weaver.general.GCONST;
import weaver.general.Util;
import weaver.integration.logging.Logger;
import weaver.integration.logging.LoggerFactory;

public class WsdlUtil {
    private static final Logger log = LoggerFactory.getLogger(WsdlUtil.class);
    private static URLClassLoader classLoader;
    private static Class clazz;
    private static String WSDL_ROOT;
    public static final String SOAPACTION = "SOAPAction";
    public static final String LOCALTION = "localtion";

    public WsdlUtil() {
    }

    private static void init() {
        if (classLoader != null) {
            try {
                classLoader.close();
                log.error("ClassLoader重新初始化了");
            } catch (IOException var2) {
                log.error(JavaUtil.getExceptionDetail(var2));
            }
        }

        classLoader = JavaUtil.getJarLoader("0", WSDL_ROOT, "");

        try {
            clazz = Class.forName("com.weaver.soapui.SoapUIHelper", Boolean.TRUE, classLoader);
        } catch (ClassNotFoundException var1) {
            var1.printStackTrace();
            log.error(JavaUtil.getExceptionDetail(var1));
        }

    }

    private static Object getSoapUIHelper() throws Exception {
        if (clazz == null) {
            clazz = Class.forName("com.weaver.soapui.SoapUIHelper", Boolean.TRUE, classLoader);
        }

        Constructor var0 = clazz.getDeclaredConstructor(String.class);
        return var0.newInstance(WSDL_ROOT);
    }

    private static Object setAuth(AuthBean var0) throws Exception {
        Object var1 = getSoapUIHelper();
        if (var0 != null && StringUtils.isNotBlank(var0.getAuthType())) {
            Method var2 = clazz.getMethod("setAuthData", String.class, String.class, String.class, String.class);
            var2.invoke(var1, var0.getAuthType(), var0.getUserName(), var0.getPassword(), var0.getDomain());
        }

        return var1;
    }

    public static void clearWsdl(String var0) throws Exception {
        try {
            ClassLoaderSwapper var1 = ClassLoaderSwapper.newCurrentThreadClassLoaderSwapper();
            var1.setCurrentThreadClassLoader(classLoader);
            Object var2 = getSoapUIHelper();
            Method var3 = clazz.getMethod("removeCache", String.class);
            var3.invoke(var2, getFullWsdlPath(var0));
        } catch (InvocationTargetException var4) {
            log.error(JavaUtil.getExceptionDetail(var4.getTargetException()));
        }
    }

    public static List<WsdlBean> analyticWsdl(String var0, AuthBean var1) throws Exception {
        boolean var2 = false;

        while(true) {
            try {
                ClassLoaderSwapper var3 = ClassLoaderSwapper.newCurrentThreadClassLoaderSwapper();
                var3.setCurrentThreadClassLoader(classLoader);
                ArrayList var4 = new ArrayList();
                Object var5 = setAuth(var1);
                Method var6 = clazz.getMethod("analyticWsdl", String.class);
                Object var7 = var6.invoke(var5, getFullWsdlPath(var0));
                var3.restoreCurrentThreadClassLoader();
                List var8 = (List)var7;
                Iterator var10 = var8.iterator();

                while(var10.hasNext()) {
                    Map var11 = (Map)var10.next();
                    WsdlBean var9 = new WsdlBean();
                    var9.setSoapVersion(Util.null2String(var11.get("soapVersion")));
                    List var12 = (List)var11.get("soapOperation");
                    Iterator var13 = var12.iterator();

                    while(var13.hasNext()) {
                        String var14 = (String)var13.next();
                        var9.addMethod(var14);
                    }

                    var4.add(var9);
                }

                return var4;
            } catch (InvocationTargetException var15) {
                log.error(JavaUtil.getExceptionDetail(var15.getTargetException()));
                if (var2) {
                    throw var15;
                }

                clearWsdl(getFullWsdlPath(var0));
                var2 = !var2;
                if (!var2) {
                    return new ArrayList();
                }
            }
        }
    }

    public static Map<String, String> analyticMethod(String var0, String var1, String var2, AuthBean var3) throws Exception {
        boolean var4 = false;

        while(true) {
            try {
                ClassLoaderSwapper var5 = ClassLoaderSwapper.newCurrentThreadClassLoaderSwapper();
                var5.setCurrentThreadClassLoader(classLoader);
                Object var6 = setAuth(var3);
                Method var7 = clazz.getMethod("getDataTemplate", String.class, String.class, String.class);
                Object var8 = var7.invoke(var6, getFullWsdlPath(var0), var1, var2);
                var5.restoreCurrentThreadClassLoader();
                Map var9 = (Map)var8;
                return var9;
            } catch (InvocationTargetException var10) {
                log.error(JavaUtil.getExceptionDetail(var10.getTargetException()));
                if (var4) {
                    throw var10;
                }

                clearWsdl(getFullWsdlPath(var0));
                var4 = !var4;
                if (!var4) {
                    return new HashMap();
                }
            }
        }
    }

    public static Map<String, String> getRequestUrl(String var0, String var1, String var2) throws WSDLException {
        String var3 = getFullWsdlPath(var0);
        WSDLFactory var4 = WSDLFactory.newInstance();
        WSDLReader var5 = var4.newWSDLReader();
        Definition var6 = var5.readWSDL(var3);
        Map var7 = var6.getAllServices();
        String var8 = null;
        HashMap var9 = new HashMap();
        Iterator var10 = var7.keySet().iterator();

        label96:
        while(var10.hasNext()) {
            QName var11 = (QName)var10.next();
            ServiceImpl var12 = (ServiceImpl)var7.get(var11);
            Map var13 = var12.getPorts();
            Iterator var14 = var13.keySet().iterator();

            while(true) {
                while(true) {
                    List var20;
                    do {
                        do {
                            if (!var14.hasNext()) {
                                continue label96;
                            }

                            String var15 = (String)var14.next();
                            Port var16 = (Port)var13.get(var15);
                            String[] var17 = new String[]{null, null, null};
                            String[] var18 = var1.split("--");

                            for(int var19 = 0; var19 < var18.length && var19 < 3; ++var19) {
                                var17[var19] = var18[var19];
                            }

                            BindingOperation var26 = var16.getBinding().getBindingOperation(var17[0], var17[1], var17[2]);
                            if (var26 != null) {
                                var20 = var26.getExtensibilityElements();
                                if (var20 != null) {
                                    for(int var21 = 0; var21 < var20.size(); ++var21) {
                                        ExtensibilityElement var22 = (ExtensibilityElement)var20.get(var21);
                                        if (var22 instanceof SOAPOperation) {
                                            SOAPOperation var23 = (SOAPOperation)var22;
                                            var9.put("SOAPAction", var23.getSoapActionURI());
                                            break;
                                        }
                                    }
                                }
                            }

                            var20 = var16.getExtensibilityElements();
                        } while(var20 == null);
                    } while(var20.size() <= 0);

                    Iterator var27 = var20.iterator();

                    while(var27.hasNext()) {
                        Object var28 = var27.next();
                        SOAP12Address var29;
                        SOAPAddress var30;
                        if ("SOAP 1.1".equalsIgnoreCase(var2)) {
                            if (var28 instanceof SOAPAddress) {
                                var30 = (SOAPAddress)var28;
                                var9.put("localtion", var30.getLocationURI());
                                var8 = null;
                                break;
                            }

                            try {
                                var29 = (SOAP12Address)var28;
                                var8 = var29.getLocationURI();
                            } catch (Exception var25) {
                            }
                        } else if ("SOAP 1.2".equalsIgnoreCase(var2)) {
                            if (var28 instanceof SOAP12Address) {
                                var29 = (SOAP12Address)var28;
                                var9.put("localtion", var29.getLocationURI());
                                var8 = null;
                                break;
                            }

                            try {
                                var30 = (SOAPAddress)var28;
                                var8 = var30.getLocationURI();
                            } catch (Exception var24) {
                            }
                        }
                    }
                }
            }
        }

        if (var8 != null) {
            var9.put("localtion", var8);
        }

        return var9;
    }

    public static Response submitDataForHttp(String var0, String var1, String var2, String var3, AuthBean var4, long var5, String var7) throws Exception {
        Response var8 = new Response();
        int var9 = (new Long(var5)).intValue();
        RequestConfig var10 = RequestConfig.custom().setConnectionRequestTimeout(var9).setConnectTimeout(var9).setSocketTimeout(var9).build();
        HttpRequestRetryHandler var11 = (var0x, var1x, var2x) -> {
            if (var1x >= 3) {
                return false;
            } else if (var0x instanceof NoHttpResponseException) {
                return false;
            } else if (var0x instanceof SSLHandshakeException) {
                return false;
            } else if (var0x instanceof InterruptedIOException) {
                return false;
            } else if (var0x instanceof UnknownHostException) {
                return false;
            } else if (var0x instanceof ConnectTimeoutException) {
                return false;
            } else if (var0x instanceof SSLException) {
                return false;
            } else {
                HttpClientContext var32 = HttpClientContext.adapt(var2x);
                HttpRequest var42 = var32.getRequest();
                return !(var4 instanceof HttpEntityEnclosingRequest);
            }
        };
        SSLContext var12 = SSLContext.getInstance("TLSv1.2");
        X509TrustManager var13 = new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkServerTrusted(X509Certificate[] var1, String var2) {
            }

            public void checkClientTrusted(X509Certificate[] var1, String var2) {
            }
        };
        var12.init((KeyManager[])null, new TrustManager[]{var13}, (SecureRandom)null);
        CloseableHttpClient var14 = HttpClients.custom().setDefaultRequestConfig(var10).setRetryHandler(var11).setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).setSslcontext(var12).build();
        HttpPost var15 = new HttpPost(var0);
        StringEntity var16 = new StringEntity(var3, var7);
        if ("SOAP 1.2".equalsIgnoreCase(var2)) {
            var16.setContentType("application/soap+xml;charset=" + var7);
        } else {
            var16.setContentType("text/xml;charset=" + var7);
            var15.addHeader("SOAPAction", Util.null2String(var1));
        }

        var15.setEntity(var16);
        var15.setConfig(var10);
        if (var4 != null && StringUtils.equalsIgnoreCase("Basic", var4.getAuthType())) {
            var15.addHeader("Authorization", SignatureUtil.getBasicSign(var4.getUserName(), var4.getPassword()));
        }

        CloseableHttpResponse var17 = var14.execute(var15);
        int var18 = var17.getStatusLine().getStatusCode();
        if (var18 == 200) {
            var8.setStatus(Boolean.TRUE);
        } else {
            var8.setStatus(Boolean.FALSE);
        }

        HttpEntity var19 = var17.getEntity();
        StringBuilder var20 = new StringBuilder();
        if (var19 != null && var19.getContent() != null) {
            BufferedReader var21 = new BufferedReader(new InputStreamReader(var19.getContent(), var7));

            String var22;
            while((var22 = var21.readLine()) != null) {
                var20.append(var22);
            }
        }

       // Console.log("请求:"+ var15.getEntity().getContent().toString());
        String Res=var20.toString().replace(".Response","Response");
        Res=var20.toString().replace("getDepartIdResponse","getDepartId__Response");

        Console.log("retrun:"+Res);

        var8.setResponseString(Res);
        return var8;
    }

    public static Response submitData(String var0, String var1, String var2, String var3, AuthBean var4, long var5, String var7) throws Exception {
        Response var8 = new Response();
        if (var4 != null && !StringUtils.isBlank(var4.getAuthType()) && !StringUtils.equalsIgnoreCase("Basic", var4.getAuthType())) {
            boolean var16 = false;

            while(true) {
                try {
                    ClassLoaderSwapper var10 = ClassLoaderSwapper.newCurrentThreadClassLoaderSwapper();
                    var10.setCurrentThreadClassLoader(classLoader);
                    Object var17 = setAuth(var4);
                    Method var12 = clazz.getMethod("submitData", String.class, String.class, String.class, String.class, String.class);
                    Object var13 = var12.invoke(var17, getFullWsdlPath(var0), var1, var2, var3, Util.null2String(var5 + "", ""));
                    Map var14 = (Map)var13;
                    if (StringUtils.equals("1", (String)var14.get("status"))) {
                        var8.setStatus(Boolean.TRUE);
                    } else {
                        var8.setStatus(Boolean.FALSE);
                    }

                    var8.setResponseString((String)var14.get("content"));
                    var10.restoreCurrentThreadClassLoader();
                    return var8;
                } catch (InvocationTargetException var15) {
                    String var11 = JavaUtil.getExceptionDetail(var15.getTargetException());
                    log.error(var11);
                    var8.setStatus(Boolean.FALSE);
                    var8.setResponseString(var11);
                    if (var16) {
                        throw var15;
                    }

                    clearWsdl(getFullWsdlPath(var0));
                    var16 = !var16;
                    if (!var16) {
                        break;
                    }
                }
            }
        } else {
            Map var9 = getRequestUrl(var0, var2, var1);
            var8 = submitDataForHttp((String)var9.get("localtion"), (String)var9.get("SOAPAction"), var1, var3, var4, var5, var7);
        }

        return var8;
    }

    private static String getFullWsdlPath(String var0) {
        return EsbConstant.ESB_WSDL_PATH + var0;
    }

    public static List<ParamBean> readParamsXml(String var0, TransmitType var1) throws DocumentException {
        Document var2 = DocumentHelper.parseText(var0);
        Element var3 = var2.getRootElement();
        JSONObject var4 = new JSONObject();
        var4.put("prefix", var3.getNamespacePrefix());
        var4.put("uri", var3.getNamespaceURI());
        var4.put("spacetype", UUID.randomUUID().toString());
        ArrayList var5 = new ArrayList();
        ParamBean var6 = new ParamBean();
        var6.setParamKey(var3.getName());
        var6.setParamName(var3.getName());
        var6.setTransmitType(var1.getValue());
        var6.setArrs(Boolean.FALSE);
        var6.setRequired(Boolean.TRUE);
        var6.setDataType(DataType.NODE.getValue());
        var6.setParamType(ParamType.XML.getValue());
        var6.setExt(var4.toJSONString());
        var5.add(var6);
        List var7 = var3.attributes();
        if (var7 != null && var7.isEmpty()) {
            var5.addAll(readAttribute(var3.getName(), var1, var7));
        }

        if (var3.isTextOnly()) {
            if ("?".equals(var3.getText())) {
                return var5;
            }
        } else {
            var5.addAll(readElement(var3.getName(), var1, var3));
        }

        return var5;
    }

    private static List<ParamBean> readAttribute(String var0, TransmitType var1, List<Attribute> var2) {
        ArrayList var3 = new ArrayList();

        for(int var5 = 0; var5 < var2.size(); ++var5) {
            Attribute var6 = (Attribute)var2.get(var5);
            ParamBean var4 = new ParamBean();
            var4.setParamName(var6.getName());
            var4.setParamKey(var0 + "." + var6.getName());
            var4.setParentName(var0);
            var4.setTransmitType(var1.getValue());
            var4.setArrs(Boolean.FALSE);
            var4.setRequired(Boolean.TRUE);
            var4.setDataType(DataType.ATTRIBUTE.getValue());
            var4.setParamType(ParamType.STRING.getValue());
            var3.add(var4);
        }

        return var3;
    }

    private static List<ParamBean> readElement(String var0, TransmitType var1, Element var2) {
        ArrayList var3 = new ArrayList();
        ParamBean var4 = null;
        int var5 = 0;

        for(int var6 = var2.nodeCount(); var5 < var6; ++var5) {
            if (var4 == null) {
                var4 = new ParamBean();
                var4.setArrs(Boolean.FALSE);
                var4.setRequired(Boolean.TRUE);
                var4.setParamType(ParamType.STRING.getValue());
            }

            Node var7 = var2.node(var5);
            if (!(var7 instanceof Element)) {
                if (var7 instanceof Comment) {
                    Comment var17 = (Comment)var7;
                    String var18 = Util.null2String(var17.getText());
                    if (var18.startsWith("type:")) {
                        var18 = var18.substring(5).trim();
                        var4.setParamType(ParamType.newInstance(var18).getValue());
                    } else if ("Zero or more repetitions:".equalsIgnoreCase(var18)) {
                        var4.setArrs(Boolean.TRUE);
                    } else if ("Optional:".equalsIgnoreCase(var18)) {
                        var4.setRequired(Boolean.FALSE);
                    }
                }
            } else {
                Element var8 = (Element)var7;
                JSONObject var9 = new JSONObject();
                var9.put("prefix", var8.getNamespacePrefix());
                var9.put("uri", var8.getNamespaceURI());
                var4.setExt(var9.toJSONString());
                var4.setParamName(var8.getName());
                var4.setParamKey(var0 + "." + var8.getName());
                var4.setParentName(var0);
                var4.setTransmitType(var1.getValue());
                var4.setDataType(DataType.NODE.getValue());
                List var10 = var8.attributes();
                String var11;
                if (var8.isTextOnly()) {
                    if (var10 != null && !var10.isEmpty()) {
                        var11 = var4.getParamType();
                        var4.setParamType(ParamType.XML.getValue());
                        var3.add(var4);
                        ParamBean var12 = new ParamBean();
                        var12.setArrs(var4.isArrs());
                        var12.setRequired(var4.isRequired());
                        var12.setParamType(var11);
                        var12.setDataType(DataType.CONTENT.getValue());
                        var12.setTransmitType(var1.getValue());
                        var12.setParentName(var4.getParamKey());
                        StringBuilder var13 = new StringBuilder();

                        for(int var14 = 0; var14 < var10.size(); ++var14) {
                            Attribute var15 = (Attribute)var10.get(var14);
                            var13.append(",").append(var15.getName()).append(",");
                        }

                        String var21 = "text";
                        if (StringUtils.contains(var13.toString(), ",text,")) {
                            var21 = var21 + "_" + RandomUtil.generateString(4);
                        }

                        var12.setParamName(var21);
                        var12.setParamKey(var4.getParamKey() + "." + var21);
                        var3.add(var12);
                    } else {
                        var3.add(var4);
                    }
                } else {
                    var11 = var4.getParamType();
                    var4.setParamType(ParamType.XML.getValue());
                    var3.add(var4);
                    List var19 = readElement(var4.getParamKey(), var1, var8);
                    if (var19 != null && !var19.isEmpty()) {
                        var3.addAll(var19);
                    } else if (var10 != null && !var10.isEmpty()) {
                        ParamBean var20 = new ParamBean();
                        var20.setArrs(var4.isArrs());
                        var20.setRequired(var4.isRequired());
                        var20.setParamType(var11);
                        var20.setDataType(DataType.CONTENT.getValue());
                        var20.setTransmitType(var1.getValue());
                        var20.setParentName(var4.getParamKey());
                        StringBuilder var23 = new StringBuilder();

                        for(int var22 = 0; var22 < var10.size(); ++var22) {
                            Attribute var16 = (Attribute)var10.get(var22);
                            var23.append(",").append(var16.getName()).append(",");
                        }

                        String var24 = "text";
                        if (StringUtils.contains(var23.toString(), ",text,")) {
                            var24 = var24 + "_" + RandomUtil.generateString(4);
                        }

                        var20.setParamName(var24);
                        var20.setParamKey(var4.getParamKey() + "." + var24);
                        var3.add(var20);
                    } else {
                        var4.setParamType(var11);
                    }
                }

                if (var10 != null && !var10.isEmpty()) {
                    var3.addAll(readAttribute(var4.getParamKey(), var1, var10));
                }

                var4 = null;
            }
        }

        return var3;
    }

    static {
        WSDL_ROOT = GCONST.getRootPath() + "WEB-INF" + File.separatorChar + "lib-soapui";
        init();
    }
}
