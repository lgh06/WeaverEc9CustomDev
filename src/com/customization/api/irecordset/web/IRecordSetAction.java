package com.customization.api.irecordset.web;


import com.alibaba.fastjson.JSONObject;
import com.customization.api.irecordset.service.IRecordSetService;
import com.customization.api.irecordset.service.impl.IRecordSetServiceImpl;
import com.engine.common.util.ParamUtil;
import com.engine.common.util.ServiceUtil;
import weaver.hrm.HrmUserVarify;
import weaver.hrm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

public class IRecordSetAction {


    private IRecordSetService getService(User user) {
        return ServiceUtil.getService(IRecordSetServiceImpl.class, user);
    }

    @GET
    @Path("/data")
    @Produces({MediaType.TEXT_PLAIN})
    public String weatableDemo(@Context HttpServletRequest request, @Context HttpServletResponse response){
        Map<String, Object> apidatas = new HashMap<String, Object>();
        try {
            //获取当前用户
            User user = HrmUserVarify.getUser(request, response);

                apidatas.putAll(getService(user).data(ParamUtil.request2Map(request)));
                apidatas.put("api_status", true);

        } catch (Exception e) {
            e.printStackTrace();
            apidatas.put("api_status", false);
            apidatas.put("api_errormsg", "catch exception : " + e.getMessage());
        }
        return JSONObject.toJSONString(apidatas);

    }
}
