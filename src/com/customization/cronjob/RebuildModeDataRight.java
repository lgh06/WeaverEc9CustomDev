package com.customization.cronjob;

import com.customization.commons.Console;
import weaver.conn.RecordSet;
import weaver.interfaces.schedule.BaseCronJob;

import java.util.UUID;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName SynToDoSomething.java
 * @Description TODO
 * @createTime 2020-06-02 8:45:00
 */
public class RebuildModeDataRight extends BaseCronJob {


    @Override
    public void execute() {

        String dothingName = "RebuildModeDataRight";
        String uuid = UUID.randomUUID().toString().replace("-", "");
        Console.log(" execute BaseCronJob " + dothingName + " ================START================= " + uuid);


        long startreadSap = System.currentTimeMillis();


        RecordSet recordSet = new RecordSet();
        recordSet.execute("select * from syncronjob  where syntype='" + dothingName + "'");//记录当前是否已经有线程在跑
        boolean syncProcess = false;
        if (recordSet.next()) {
            if (recordSet.getInt("status") == 0) {
                syncProcess = true;
            } else {

                long lastreadSapTime = Long.parseLong(recordSet.getString("datestr"));
                long nowTime = System.currentTimeMillis();
                Console.log("lastreadSapTime：" + lastreadSapTime);
                Console.log("nowTime：" + nowTime);
                Console.log("距离上次同步时间：" + (nowTime - lastreadSapTime) / 1000 / 60);
                if ((nowTime - lastreadSapTime) / 1000 / 60 > 5) {//增加距离最后一次同步时间大于5分的重新可继续执行计划任务
                    syncProcess = true;
                } else {
                    Console.log("当前正在同步中.....  update syncronjob set status='0' ");
                    Console.log("距离上次开始任务没有超过5分钟，请等待上次任务执行完成");
                    Console.log("================END=================");
                }

            }
        } else {
            recordSet.execute("insert  into  syncronjob (status,datestr,syntype) values ('0','" + System.currentTimeMillis() + "','" + dothingName + "')");
            syncProcess = true;
        }
        if (syncProcess) {
            recordSet.execute("update syncronjob set status='1',datestr='" + System.currentTimeMillis() + "' where syntype='" + dothingName + "'");//标记为已经有线程在跑
            this.runSomeThing();
            recordSet.execute("update syncronjob set status='0' where syntype='" + dothingName + "'");//标记当前同步进程已经结束
            Console.log("整体耗时：" + (System.currentTimeMillis() - startreadSap) / 1000 + "s");
            Console.log("================END=================");
        }
        Console.log(" execute BaseCronJob " + dothingName + " ================END================= " + uuid);


    }

    /**
     * 计划任务实际执行的方法体
     */
    public void runSomeThing() {

        RecordSet rs = new RecordSet();
        RecordSet recordSet = new RecordSet();

        rs.execute("select top 10000 id,formmodeId from uf_yjfktz  where formmodeid is not null  and qxzgzt ='0' order by id  desc");
        Console.log("本次权限重构数量:" + rs.getCounts());
        int id = 0;
        while (rs.next()) {
            int formmodeid = rs.getInt("formmodeId");
            int billid = rs.getInt("id");
            weaver.formmode.setup.ModeRightInfo ModeRightInfo = new weaver.formmode.setup.ModeRightInfo();
            ModeRightInfo.rebuildModeDataShareByEdit(1, formmodeid, billid);
            id++;
            if (id / 100 == 0) {
                recordSet.execute("update syncronjob set status='1',datestr='" + System.currentTimeMillis() + "' where syntype='RebuildModeDataRight'");//标记为已经有线程在跑
            }
            //更新当前重构标识的数据
            recordSet.execute("update uf_yjfktz  set qxzgzt = '1' where id ='" + billid + "'");
        }


    }


}
