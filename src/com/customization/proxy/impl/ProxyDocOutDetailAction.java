package com.customization.proxy.impl;

import com.api.doc.search.util.DocSptm;
import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;

@WeaIocReplaceComponent("")
public class ProxyDocOutDetailAction {
    @WeaReplaceAfter(value = "/api/doc/out/detail/basicInfo", order = 1)
    public String afterSaveLoginId(WeaAfterReplaceParam weaAfterReplaceParam) {

        String data = weaAfterReplaceParam.getData();
        JSONObject obj = JSONObject.fromObject(data);
        obj.put("customDesc", "ProxyDocOutDetailAction 20220714 lth");
        try {

            DocSptm var4 = new DocSptm();
            int docid = var4.getOutDocId(weaAfterReplaceParam.getParamMap().get("id").toString());
            obj.put("cus_docid", "" + docid);
            RecordSet rs = new RecordSet();
            rs.execute(" select docimagefile.imagefileid as imagefileid,filerealpath,imagefiletype,imagefile.imagefilename,iszip from imagefile,docimagefile "
                    + " where imagefile.imagefileid =docimagefile.imagefileid  and  docimagefile.docid='" + docid + "' and  docimagefile.docfiletype not in(1,11) ");
            obj.put("cus_attacmentCount", "" + rs.getCounts());
            rs.execute("select seccategory from  docdetail  where id ='" + docid + "'");
            if (rs.first()) {
                obj.put("cus_seccategory", "" + rs.getString("seccategory"));
            }

        } catch (Exception e) {

        }

        return obj.toString();
    }

}
