package com.customization.api.uddi.entity;

import java.util.List;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName WSDetailTable.java
 * @Description 明细表
 * @createTime 2019-08-04 12:57:00
 */
public  class WSDetailTable {
    private Integer dtIndex;
    private List<WSRow> rows;

    public Integer getDtIndex() {
        return dtIndex;
    }

    public void setDtIndex(Integer dtIndex) {
        this.dtIndex = dtIndex;
    }

    public List<WSRow> getRows() {
        return rows;
    }

    public void setRows(List<WSRow> rows) {
        this.rows = rows;
    }
}
