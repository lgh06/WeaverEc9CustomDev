package com.customization.api.uddi.service.impl;

import com.customization.api.uddi.cmd.CreateRequestCmd;
import com.customization.api.uddi.cmd.GenerateWsdlCmd;
import com.customization.api.uddi.service.UddiService;
import com.engine.core.impl.Service;

import weaver.hrm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class UddiServiceImpl extends Service implements UddiService {

    @Override
    public String getWsdl(Map<String, Object> params, User user) {
        return commandExecutor.execute(new GenerateWsdlCmd(params,user)).get("result").toString();
    }

    @Override
    public String createRequest(HttpServletRequest request,HttpServletResponse response) {
        return commandExecutor.execute(new CreateRequestCmd(request,response)).get("result").toString();
    }



}
