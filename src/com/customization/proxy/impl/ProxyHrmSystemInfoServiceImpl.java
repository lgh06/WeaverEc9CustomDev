package com.customization.proxy.impl;


import com.customization.commons.Console;
import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import weaver.conn.RecordSet;
import weaver.upgradetool.wscheck.Util;

import java.util.Base64;
import java.util.Map;

@WeaIocReplaceComponent("HrmSystemInfoService")
public class ProxyHrmSystemInfoServiceImpl {




    @WeaReplaceAfter(value = "/api/hrm/systeminfo/save", order = 1, description = "系统信息保存后更新公司邮箱和登录名保持一致")
    public String afterHrmSystemInfoServiceSave(WeaAfterReplaceParam weaAfterReplaceParam){

        JSONObject json = JSONObject.fromObject(weaAfterReplaceParam.getData());


        Map params=weaAfterReplaceParam.getParamMap();

        String loginid = Util.null2String(params.get("loginid"));
        String id = params.get("id").toString();//原来的ID
        new Thread((new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.currentThread().sleep(5000);

                    if(!loginid.equals("")){
                        String email=loginid+"@angelyeast.com";
                        String updSql="update  hrmresource set email='"+email+"' where id='"+id+"'";
                        boolean b= new RecordSet().execute(updSql);
                        Console.log("SystemInfoService updSql:" + updSql + "  ; 执行结果:" + b);
                    }
                }catch (Exception e){

                }
            }
        })).start();

        json.put("Proxy_desc","系统信息保存后更新公司邮箱和登录名保持一致，代理类ProxyHrmSystemInfoServiceImpl");
        return json.toString();


    }
}
