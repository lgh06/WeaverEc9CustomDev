package com.customization.api.uddi.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName WSField.java
 * @Description 流程字段
 * @createTime 2019-08-04 12:55:00
 */
public class WSField {

    private String name;//数据库字段
    private String value;//值
    private String type;//类型
    private String description;//字段汉字描述

    private List<WSFile> files;

    public WSField() {}

    public WSField(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public WSField(String name, String value, String type) {
        this.name = name;
        this.value = value;
        this.type = type;
    }

    public WSField(String name, String value, String type, String description) {
        this.name = name;
        this.value = value;
        this.type = type;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value == null ? "" : value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<WSFile> getFiles() {
        if (files == null) {
            files = new ArrayList<WSFile>();
        }
        return files;
    }

    public void setFiles(List<WSFile> files) {
        this.files = files;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
