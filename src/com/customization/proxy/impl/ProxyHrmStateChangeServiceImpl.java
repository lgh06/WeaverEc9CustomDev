package com.customization.proxy.impl;


import com.customization.commons.Console;
import com.weaverboot.frame.ioc.anno.classAnno.WeaIocReplaceComponent;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceAfter;
import com.weaverboot.frame.ioc.anno.methodAnno.WeaReplaceBefore;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaAfterReplaceParam;
import com.weaverboot.frame.ioc.handler.replace.weaReplaceParam.impl.WeaBeforeReplaceParam;
import net.sf.json.JSONObject;
import tebie.applib.api.APIContext;
import tebie.applib.api.IClient;
import weaver.common.StringUtil;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.interfaces.email.CoreMailUtil;


import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * 创建临时表结构
 create  table hreresource_history_angel
 (
 id varchar2(20),
 lastname varchar2(1000),
 email varchar2(1000),
 loginid varchar2(30),
 dismissDate varchar2(20),
 workcode varchar2(100)
 )
 */

@WeaIocReplaceComponent("HrmStateChangeService")
public class ProxyHrmStateChangeServiceImpl {


    @WeaReplaceAfter(value = "/api/hrm/statechange/saveHrmDismiss", order = 1, description = "修复开了coremail集成后人员类之后清空email的问题")
    public String afterSaveHrmDismiss(WeaAfterReplaceParam weaAfterReplaceParam){

        JSONObject json = JSONObject.fromObject(weaAfterReplaceParam.getData());


        Map params=weaAfterReplaceParam.getParamMap();
        Console.log(" afterSaveHrmDismiss ParamMap:"+params.toString());
        RecordSet rst=new RecordSet();
        RecordSet temprs=new RecordSet();
        String tempresourceid = StringUtil.vString(params.get("tempresourceid"));
        String secSQL = "select * from hreresource_history_angel where " + weaver.general.Util.getSubINClause(tempresourceid, "id", "in");
        rst.execute(secSQL);
        while(rst.next()){
            String id =  Util.null2String(rst.getString("id"));
            String loginid = Util.null2String(rst.getString("loginid"));
            String email = Util.null2String(rst.getString("email"));
            String lastname = Util.null2String(rst.getString("lastname"));
            String tmpsql = "update hrmresource set loginid='"+loginid+"',email='"+email+"' where id="+id;
            Console.log("回写登录名和email:"+tmpsql);
            temprs.execute(tmpsql);
            deleCoreMailUser(lastname,email);

        }

        json.put("Proxy_after_desc","离职接口处理完成后，回写email,loginid，代理类 ProxyHrmStateChangeServiceImpl");
        return json.toString();

    }

    @WeaReplaceBefore(value = "/api/hrm/statechange/saveHrmDismiss", order = 1, description = "离职时备份登录名和密码")
    public void beforeSaveHrmDismiss(WeaBeforeReplaceParam weaBeforeReplaceParam){
        Map<String,Object> params=weaBeforeReplaceParam.getParamMap();
        Console.log(" beforeSaveHrmDismiss ParamMap:"+params.toString());
        String tempresourceid = StringUtil.vString(params.get("tempresourceid"));
        String dismissDate = StringUtil.vString(params.get("changedate"));
        String secSQL = "select * from hrmresource where " + weaver.general.Util.getSubINClause(tempresourceid, "id", "in");
        RecordSet rst=new RecordSet();
        RecordSet temprs=new RecordSet();
        rst.execute(secSQL);
        while(rst.next()){
            String id =  Util.null2String(rst.getString("id"));
            String workcode = Util.null2String(rst.getString("workcode"));
            String loginid = Util.null2String(rst.getString("loginid"));
            String email = Util.null2String(rst.getString("email"));
            String lastname = Util.null2String(rst.getString("lastname"));
            temprs.execute("select * from  hreresource_history_angel  where id='"+id+"'");

            String tmpsql ="";
            if(temprs.first()){
                tmpsql = "update hreresource_history_angel set  lastname='"+lastname+"', workcode='"+workcode+"',loginid='"+loginid+"',email='"+email+"',dismissDate='"+dismissDate+"' where id="+id;
            }else{
                tmpsql = "insert into hreresource_history_angel(id,workcode,loginid,email,dismissDate,lastname)  values('"+id+"','"+workcode+"','"+loginid+"','"+email+"','"+dismissDate+"','"+lastname+"')";
            }
            Console.log("备份登录名和email "+tmpsql);
            temprs.execute(tmpsql);
        }

    }

    public void deleCoreMailUser(String lastname,String email) {
        Socket socket_user = null;
        IClient client_user = null;
        CoreMailUtil coremailUtil = new CoreMailUtil();

        RecordSet rs = new RecordSet();

        String sql = "select * from coremailsetting";
        rs.execute(sql);
        while (rs.next()) {

            String systemAddress = Util.null2String(rs.getString("systemaddress"));
            int emailPort = Util.getIntValue(rs.getString("emailPort"), 6195);
            try {
                socket_user = new Socket(systemAddress, emailPort);
                client_user = APIContext.getClient(socket_user);
                client_user.deleteUser(email);
                coremailUtil.insertCoreMailSynLog("3", lastname, "2", "1", "延迟删除CoreMail账户");
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
