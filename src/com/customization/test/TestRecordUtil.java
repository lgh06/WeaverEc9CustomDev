package com.customization.test;


import com.customization.commons.Console;

import com.customization.commons.LanguageUtil;
import com.google.gson.JsonObject;
import net.sf.json.JSONObject;
import okhttp3.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;
import weaver.conn.RecordSet;
import weaver.general.StaticObj;
import weaver.interfaces.datasource.DataSource;
import weaver.security.core.MD5;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName TestRecordUtil.java
 * @Description 本地调用服务器的实例,
 * @createTime 2020-04-28 14:31:00
 */
public class TestRecordUtil extends BaseTest {







    @Test
    public void withDataSource() {
        String sql = "select  lastname,password from hrmresource";
        //调用数据源生成jdbc链接   //localhost为配置的数据源标识
        DataSource ds = (DataSource) StaticObj.getServiceByFullname(("datasource.localhost"), DataSource.class);
        Connection conn = null;
        try {
            conn = ds.getConnection();
            ResultSet rs = conn.createStatement().executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                if (i == 10) {
                    break;
                }
                i++;
                System.out.println("name-->" + rs.getString("lastname") + " pwd-->" + rs.getString("password"));
            }
            rs.close();
        } catch (Exception e) {

        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
            }
        }
    }

    @Test
    public void withlicense() {
        String sql = " select COMPANYNAME,LICENSE,EXPIREDATE,CVERSION from  license ";
        RecordSet rs = new RecordSet();
        rs.executeQuery(sql);

        if (rs.next()) {
            System.out.println("公司名称:" + rs.getString(1));
            System.out.println("LICENSE:" + rs.getString(2));
            System.out.println("授权到期日期:" + rs.getString(3));
            System.out.println("版本:" + rs.getString(4));
            Console.log(sql);
        }

    }

    @Test
    public void with() {

        LanguageUtil.Translate("workflow_nodegroup","groupname");//翻译节点操作者组
        LanguageUtil.Translate("workflow_nodebase","nodename");//翻译节点名称
      /*
        LanguageUtil.TranslateHtmlLabelInfo(7,8);//翻译多语言标签库
        LanguageUtil.Translate("HrmSubCompany","subcompanyname");//翻译分部名称
        LanguageUtil.Translate("HrmSubCompany","subcompanydesc");//翻译分部名称
        LanguageUtil.Translate("workflow_SelectItem","selectname");//翻译选择框
        LanguageUtil.Translate("hrmresource","lastname");////翻译姓名
        LanguageUtil.Translate("hrmdepartment","departmentmark");//翻译部门名称
        LanguageUtil.Translate("hrmdepartment","departmentname");//翻译部门名称
        LanguageUtil.Translate("leftmenuinfo","customName");//翻译二级菜单
    */
    }




}
