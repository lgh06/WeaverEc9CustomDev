package com.customization.action.demo;

import weaver.general.Util;
import weaver.hrm.User;
import weaver.interfaces.workflow.action.Action;
import weaver.soa.workflow.request.Cell;
import weaver.soa.workflow.request.DetailTable;
import weaver.soa.workflow.request.Property;
import weaver.soa.workflow.request.RequestInfo;
import weaver.soa.workflow.request.Row;
import weaver.workflow.request.RequestManager;

import javax.servlet.http.HttpServletRequest;


public class DevActionDemo implements Action {
    /**
     * 自定义参数
     */
    public String defaultParam;

    @Override
    public String execute(RequestInfo requestInfo) {
        RequestManager requestManager = requestInfo.getRequestManager();
        HttpServletRequest request = requestManager.getRequest();

        String requestId = requestInfo.getRequestid();//请求ID
        String reqId = Util.null2String(request.getParameter("requestid"));//请求ID
        String requestLevel = requestInfo.getRequestlevel();//请求紧急程度
        //当前操作类型 submit:提交/reject:退回
        String src = requestManager.getSrc();
        String workflowId = requestInfo.getWorkflowid();//流程ID
        String tableName = requestManager.getBillTableName();//表单名称
        int billId = requestManager.getBillid();//表单数据ID
        User usr = requestManager.getUser();//获取当前操作用户对象
        String requestName = requestManager.getRequestname();//请求标题
        String remark = requestManager.getRemark();//当前用户提交时的签字意见
        int formId = requestManager.getFormid();//表单ID
        int isBill = requestManager.getIsbill();//是否是自定义表单

        //取主表数据
        Property[] properties = requestInfo.getMainTableInfo().getProperty();// 获取表单主字段信息
        for (Property property : properties) {
            String name = property.getName();// 主字段名称
            String value = Util.null2String(property.getValue());// 主字段对应的值
        }
        //取明细数据
        DetailTable[] detailTable = requestInfo.getDetailTableInfo().getDetailTable();// 获取所有明细表
        if (detailTable.length > 0) {
            // 指定明细表
            for (DetailTable dt : detailTable) {
                Row[] s = dt.getRow();// 当前明细表的所有数据,按行存储
                // 指定行
                for (Row r : s) {
                    Cell[] c = r.getCell();// 每行数据再按列存储
                    // 指定列
                    for (Cell c1 : c) {
                        String name = c1.getName();// 明细字段名称
                        String value = c1.getValue();// 明细字段的值
                    }
                }
            }
        }
        //控制流程流转，增加以下两行，流程不会向下流转，表单上显示返回的自定义错误信息
        requestManager.setMessagecontent("返回自定义的错误信息");
        requestManager.setMessageid("错误信息编号");

        // return返回固定返回`SUCCESS`,
        // 当return `FAILURE_AND_CONTINUE`时，表单上会提示附加操作失败
        return SUCCESS;
    }


    public String getDefaultParam() {
        return defaultParam;
    }

    public void setDefaultParam(String defaultParam) {
        this.defaultParam = defaultParam;
    }
}
