package com.customization.api.uddi.entity;

import java.util.List;

/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName WSRow.java
 * @Description 明细表行的字段
 * @createTime 2019-08-04 12:56:00
 */
public  class WSRow {
    private List<WSField> fields;

    public List<WSField> getFields() {
        return fields;
    }

    public void setFields(List<WSField> fields) {
        this.fields = fields;
    }
}