package com.customization.example;

import weaver.conn.RecordSet;
import weaver.general.GCONST;

import java.util.Arrays;
import java.util.List;


/**
 * @author liutaihong
 * @version 1.0.0
 * @ClassName DemoApplication.java
 * @Description 获取指定目录的所有下级目录
 * @createTime 2020-06-02 9:52:01
 */
public class GetAllChildDocSecCategory {


    public void run(){


        RecordSet rs= new RecordSet();

        String secCategoryid="";
        rs.execute("select to_char(wmsys.wm_concat(id)) ids  from DocSecCategory start with id='588' connect by prior id=parentid");
        if(rs.first()){
            secCategoryid=rs.getString("ids");
        }
        System.out.println("secCategoryid:"+secCategoryid);
        System.out.println("sqlWhere:"+ getWhereInValuesSql("id",secCategoryid,3));

    }


    /**
     *
     * @param column 返回的SQL语句的列
     * @param ids  需要处理的数据集合
     * @param num 拆分成多少条    oracle最大1000个
     * @return
     */
    public static String getWhereInValuesSql(String column, String ids, int num) {
        if(ids.startsWith(",")) ids=ids.substring(1);
        if(ids.equals("")) return " 1=2 ";

        List<String> values = Arrays.asList(ids.split(","));
        // sql语句
        String sql = " (";
        // 值的个数
        int valueSize = values.size();
        // 批次数
        int batchSize = valueSize / num + (valueSize % num == 0 ? 0 : 1);
        for (int i = 0; i < batchSize; i++) {
            if (i > 0) {
                sql += ") or ";
            }
            sql += column+" in (";
            for (int j = i * num; ( j < (i + 1) * num) && j < valueSize; j++) {
                if (j > i * num) {
                    sql += ",";
                }
                sql += "" + values.get(j) + "";
            }
        }
        sql += ")) ";
        return sql;
    }





}
